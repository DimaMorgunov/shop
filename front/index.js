/**
 * @format
 */

import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import { Provider } from 'react-redux';
import { store } from './src/store/store';
import {App} from './App';
import {name as appName} from './app.json';



export class Root extends Component {
    render(){
        return(
            <Provider store={store}>
                <App />
            </Provider>
        )
    }
}

AppRegistry.registerComponent(appName, () => Root);
