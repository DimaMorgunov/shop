/**
 * @format
 */

import 'react-native';

import React from 'react';
import {Login} from '../src/Components/Auth/LoginComponent';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';


jest.mock("NativeModules", () => ({
    PlatformConstants: {
        forceTouchAvailable: false,
    },
    UIManager: {
        RCTView: () => ({
            directEventTypes: {}
        })
    },
    KeyboardObserver: {},
    RNGestureHandlerModule: {
        attachGestureHandler: jest.fn(),
        createGestureHandler: jest.fn(),
        dropGestureHandler: jest.fn(),
        updateGestureHandler: jest.fn(),
        State: {},
        Directions: {}
    },

}));


test('Login', () => {
    const snap = renderer.create(
        <Login/>
    ).toJSON();
    expect(snap).toMatchSnapshot()
});

