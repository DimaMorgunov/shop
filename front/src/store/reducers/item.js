import { handleActions } from 'redux-actions-helpers';
import * as item from '../actions/item';

export const initialState = {
    allItems: [],
    item: '',
    userItems: [],
    bin: [],
    updateItem: {}
};

export default handleActions({
    [item.setItems]: (state, action) => {
        return {
            ...state,
            allItems: action.data
        };
    },
    [item.changeField]: (state, action) => {
        return {
            ...state,
            [action.field]: action.value
        };
    },
    [item.getOneItem]: (state, action) => {
        return {
            ...state,
            error: action.error
        };
    },
    [item.setUserItems]: (state, action) => {
        return {
            ...state,
            userItems: action.data
        }
    }
}, { initialState });
