import auth from './auth';
import item from './item';
import comments from './comments';
import socket from './socket';

export default {
    auth,
    item,
    comments,
    socket
};
