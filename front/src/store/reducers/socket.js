    import { handleActions } from 'redux-actions-helpers';
import * as socket from '../actions/socket';

export const initialState = {
    message: {messages: [], members: []},
};

export default handleActions({
    [socket.setMessage]: (state, action) => {
        console.log(action)
        return {
            ...state,
            message: action.data
        }
    }
}, { initialState });
