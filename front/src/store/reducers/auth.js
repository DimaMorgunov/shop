import { handleActions } from 'redux-actions-helpers';
import * as auth from '../actions/auth';

export const initialState = {
  fetchInProcess: false,
  data: {},
  token: null,
  userList: [],
  error: {},
  success: {}
};

export default handleActions({
  [auth.changeConfirmField]: (state, action) => {
    console.log(state, action)
    return {
      ...state,
      phoneNumber: action.number,
      confirmToken: action.token,
    };
  },
  [auth.changeField]: (state, action) => {
    return {
      ...state,
      [action.field]: action.value
    };
  },
  [auth.setError]: (state, action) => {
    return {
      ...state,
      error: action.error
    };
  },
  [auth.setSuccess]: (state, action) => {
    return {
      ...state,
      success: action.success
    };
  },
  [auth.logOut]: (state) => {
    return {
      ...state,
      ...initialState
    };
  },
}, { initialState });
