import {takeEvery, put, all, call} from 'redux-saga/effects';
import * as itemActions from '../actions/item';
import * as authActions from '../actions/auth';
import * as item from '../api/item';
import * as auth from '../api/auth';

export function* getAllItems() {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const items = yield call(item.getAllItems);
        if(items.error) {throw item.message}
        yield put(itemActions.setItems(items))
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err);
        yield put(authActions.setError(err));
    }
}

export function* addNewItem({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(item.addItem, data);
        yield put(authActions.changeField('data', responseData.user.data));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err));
    }
}

export function* getUserItems() {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const user = yield call(auth.checkToken);
        if(user.error) {throw user.message}
        const userItems = yield call(item.getUserItems, user.user.data.itemList);
        yield put(itemActions.changeField('userItems', userItems));
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(user)
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err)
    }
}

export function* deleteItem({data}) {
    try{
        yield put(authActions.changeField('fetchInProcess', true));
        const user = yield call(item.deleteItem, {id: data});
        if(user.error) {throw user.message}
        yield put(authActions.setSuccess(true))
        console.log(data, 'item id')
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err)
    }
}

export function* updateItem({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(item.updateItem, {data: data});
        yield put(authActions.setSuccess(responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch(err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err))
    }
}

export function* getBinItems({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(item.getUserItems, data);
        yield put(itemActions.changeField('bin', responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err)
    }
}

export function* addToBin({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(item.addToBin, data);
        yield put(authActions.changeField('data', responseData.user.data));
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setSuccess({success: {message: 'Item has been add to bin!'}}));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err))
    }
}

export function* removeFromBin({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(item.removeFromBin, data);
        yield put(authActions.changeField('data', responseData.data));
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setSuccess({success: {message: 'Item has been removed to bin!'}}));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err))
    }
}

export function* addStar({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(item.getStarItem, data);
        yield put(itemActions.changeField('updateItem', responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err))
    }
}

export function* watchAllItems() {
    yield all([
        takeEvery(itemActions.getAllItem.toString(), getAllItems),
        takeEvery(itemActions.addItem.toString(), addNewItem),
        takeEvery(itemActions.getUserItems.toString(), getUserItems),
        takeEvery(itemActions.deleteItem.toString(), deleteItem),
        takeEvery(itemActions.updateItem.toString(), updateItem),
        takeEvery(itemActions.addToBin.toString(), addToBin),
        takeEvery(itemActions.getBinItems.toString(), getBinItems),
        takeEvery(itemActions.getStarItem.toString(), addStar),
        takeEvery(itemActions.removeFromBin.toString(), removeFromBin),
    ]);
}
