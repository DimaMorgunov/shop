import {takeEvery, put, all, call} from 'redux-saga/effects';
import * as authActions from '../actions/auth';
import * as auth from '../api/auth';
import {AsyncStorage} from "react-native";

export function* loginUser({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const credentials = yield call(auth.loginUser, data);
        yield AsyncStorage.setItem('token', JSON.stringify(credentials.user.token));
        yield put(authActions.changeField('token', credentials.user.token));
        yield put(authActions.changeField('data', credentials.user.data));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log('login Error', err);
        yield put(authActions.setError(err));
    }
}

export function* registerUser({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const credentials = yield call(auth.registerUser, data);
        yield AsyncStorage.setItem('token', JSON.stringify(credentials.user.token));
        yield put(authActions.changeField('token', credentials.user.token));
        yield put(authActions.changeField('data', credentials.user.data));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err));
    }
}

export function* authSocial({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const credentials = yield call(auth.authSocial, data);
        yield AsyncStorage.setItem('token', JSON.stringify(credentials.user.token));
        console.log(credentials)
        yield put(authActions.changeField('token', credentials.user.token));
        yield put(authActions.changeField('data', credentials.user.data));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err, 'registration Error');
        yield put(authActions.setError(err));
    }
}

export function* checkToken() {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const credentials = yield call(auth.checkToken);
        const token = yield AsyncStorage.getItem('token');
        yield put(authActions.changeField('token', token));
        yield put(authActions.changeField('data', credentials.user.data));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
    }
}

export function* sendConfirmationCode({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(auth.sendConfirmationCode, {num: data});
        yield AsyncStorage.setItem('token', JSON.stringify(responseData.user.token));
        yield put(authActions.changeField('token', responseData.user.token));
        yield put(authActions.changeField('data', responseData.user.data));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err));
    }
}

export function* getNewConfirmationCode() {
    try {
        const responseData = yield call(auth.getNewConfirmationCode);
        yield put(authActions.setSuccess(responseData))
    } catch (err) {
        yield put(authActions.setError(err))
    }
}

export function* setNewPassword({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(auth.setNewPassword, data);
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setSuccess(responseData));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err));
    }
}

export function* updateProfile({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(auth.updateProgile, data);
        yield put(authActions.changeField('data', responseData.user.data));
        yield put(authActions.setSuccess({success: {message: 'Your profile has been update'}}));
        console.log(responseData)
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
    }
}

export function* lostPassword({data}) {
 try {
     yield put(authActions.changeField('fetchInProcess', true));
     const responseData = yield call(auth.lostPassword, data);
     yield put(authActions.setSuccess(responseData));
     yield put(authActions.changeField('fetchInProcess', false));
 } catch (err) {
     yield put(authActions.changeField('fetchInProcess', false));
     yield put(authActions.setError(err))
 }
}

export function* getUsers() {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(auth.getUsers);
        yield put(authActions.changeField('userList', responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err))
    }
}

export function* editUserAdmin({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(auth.editUser, data);
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        yield put(authActions.setError(err))
    }
}


export function* logOut() {
    try {
        yield AsyncStorage.setItem('token', '');
        yield put(authActions.logOut());
    } catch (err) {
        yield put(authActions.setError(err));
    }
}

export function* watchAllLogin() {
    yield all([
        takeEvery(authActions.loginUser.toString(), loginUser),
        takeEvery(authActions.registerUser.toString(), registerUser),
        takeEvery(authActions.checkToken.toString(), checkToken),
        takeEvery(authActions.logOutAction.toString(), logOut),
        takeEvery(authActions.getNewConfirmationCode.toString(), getNewConfirmationCode),
        takeEvery(authActions.sendConfirmationCode.toString(), sendConfirmationCode),
        takeEvery(authActions.setNewPassword.toString(), setNewPassword),
        takeEvery(authActions.lostPassword.toString(), lostPassword),
        takeEvery(authActions.authSocial.toString(), authSocial),
        takeEvery(authActions.updateUser.toString(), updateProfile),
        takeEvery(authActions.getUsers.toString(), getUsers),
        takeEvery(authActions.editUserAdmin.toString(), editUserAdmin)
    ]);
}
