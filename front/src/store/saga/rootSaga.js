import {all} from 'redux-saga/effects';
import {watchAllLogin} from './auth';
import {watchAllItems} from './item';
import {watchAllComments} from './comments';

export function* rootSaga() {
    yield all([
        watchAllLogin(),
        watchAllItems(),
        watchAllComments()
    ]);
}
