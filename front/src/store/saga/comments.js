import {takeEvery, put, all, call} from 'redux-saga/effects';
import * as commentsActions from '../actions/comments';
import * as authActions from '../actions/auth';
import * as comments from '../api/commenst';

export function* getComments({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(comments.getCommets, data);
        yield put(commentsActions.setComments(responseData))
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log('get comment Error', err);
    }
}

export function* addComment({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(comments.addComment, data);
        yield put(commentsActions.setComments(responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log('add comment Error', err);
    }
}

export function* addSubComment({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(comments.addSubComment, data);
        yield put(commentsActions.setComments(responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log('add sub comment Error', err);
    }
}

export function* updateComment({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(comments.updateComment, data);
        yield put(commentsActions.setComments(responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err)
    }
}

export function* updateSubComment({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(comments.updateSubComment, data);
        yield put(commentsActions.setComments(responseData));
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err)
    }
}

export function* deleteComment({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(comments.deleteComment, data);
        yield put(commentsActions.setComments(responseData));
        console.log(responseData)
        /*yield put(commentsActions.setComments(responseData.comment));*/
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err)
    }
}

export function* deleteSubComment({data}) {
    try {
        yield put(authActions.changeField('fetchInProcess', true));
        const responseData = yield call(comments.deleteSubComment, data);
        yield put(commentsActions.setComments(responseData.comment));
        console.log(responseData)
        yield put(authActions.changeField('fetchInProcess', false));
    } catch (err) {
        yield put(authActions.changeField('fetchInProcess', false));
        console.log(err)
    }
}

export function* watchAllComments() {
    yield all([
        takeEvery(commentsActions.getComments.toString(), getComments),
        takeEvery(commentsActions.addComment.toString(), addComment),
        takeEvery(commentsActions.addSubComments.toString(), addSubComment),
        takeEvery(commentsActions.updateComment.toString(), updateComment),
        takeEvery(commentsActions.updateSubComment.toString(), updateSubComment),
        takeEvery(commentsActions.deleteComment.toString(), deleteComment),
        takeEvery(commentsActions.deleteSubComment.toString(), deleteSubComment)
    ]);
}
