import { checkStatus, get } from '../../utils/api';

export const connect = params => get('?room=', params);
