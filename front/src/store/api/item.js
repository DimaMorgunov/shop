import { checkStatus, get, post, put, del } from '../../utils/api';

export const getAllItems = data => get('api/item/all');

export const addItem = data => post('api/item/add', data);

export const getUserItems = data => post('api/item/user_items', data);

export const getStarItem = data => post('api/item/add_star', data);

export const updateItem = data => put('api/item/update', data);

export const deleteItem = data => del('api/item/delete', data);

export const addToBin = data => put('api/user/add_bin', data);

export const removeFromBin = data => put('api/user/remove_from_bin', data);