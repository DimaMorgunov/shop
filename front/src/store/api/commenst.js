import { checkStatus, get, post, put, del } from '../../utils/api';

export const getCommets = param => get('api/comments/?id=', param);

export const addComment = data => post('api/comments/add_comment', data);

export const addSubComment = data => post('api/comments/add_subcomment', data);

export const updateComment = data => put('api/comments/update_comment', data);

export const updateSubComment = data => put('api/comment/update_subcomment', data);

export const deleteComment = data => del('api/comments/delete_comment', data);

export const deleteSubComment = data => del('api/comments/delete_subcomment', data);