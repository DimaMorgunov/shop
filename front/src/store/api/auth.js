import { checkStatus, get, post, put } from '../../utils/api';

export const loginUser = data => post('api/user/login', data);

export const registerUser = data => post('api/user/registration', data);

export const updateProgile = data => put('api/user/update_profile', data);

export const getUsers = () => post('api/user/user_list');

export const editUser = (data) => put('api/user/edit_user', data);

export const authSocial = data => post('api/user/auth_social', data);

export const sendConfirmationCode = data => post('api/user/check_code', data);

export const getNewConfirmationCode = () => post('api/user/get_code');

export const setNewPassword = data => put('api/user/reset_password', data);

export const lostPassword = data => put('api/user/lost_password', data);

export const checkToken = () => post('api/user/check_token');
