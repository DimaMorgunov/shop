import { createAction } from 'redux-actions-helpers';

export const getAllItem = createAction('@item/GET_ITEMS', () => ({}));

export const getOneItem = createAction('@item/GET_ITEM', (data) => ({ data }));

export const updateItem = createAction('@item/UPDATE_ITEM', (data) => ({ data }));

export const addItem = createAction('@item/ADD_ITEM', (data) => ({ data }));

export const getBinItems = createAction('@item/GET_BIN_ITEMS', (data) => ({ data }));

export const getStarItem = createAction('@item/ADD_STAR_ITEM', (data) => ({ data }))

export const deleteItem = createAction('@item/DELETE_ITEM', (data) => ({ data }));

export const setItems = createAction('@item/SET_ITEMS', (data) => ({ data }));

export const getUserItems = createAction('@item/GET_USER_ITEMS', () => ({}));

export const setUserItems = createAction('@item/SET_USER_ITEMS', (data) => ({ data }));

export const addToBin = createAction('@item/ADD_TO_BIN', (data) => ({ data }));

export const removeFromBin = createAction('@item/REMOVE_FROM_BIN', (data) => ({data}));

export const changeField = createAction('@item/CHANGE_FIELD', (field, value) => ({ field, value }));


