import { createAction } from 'redux-actions-helpers';

export const loginUser = createAction('@auth/LOGIN_USER', (data) => ({ data }));
export const registerUser = createAction('@auth/REGISTER_USER', (data) => ({ data }));
export const authSocial = createAction('@auth/AUTH_SOCIAL', (data) => ({data}));
export const updateUser = createAction('@auth/UPDATE_AUTH', (data) => ({data}));

export const getUsers = createAction('@auth/GET_USERS', (data) => ({data}));
export const editUserAdmin = createAction('@auth/EDIT_ADMIN', (data) => ({data}));

export const checkToken = createAction('@auth/CHECK_TOKEN', () => ({}));
export const setData = createAction('@auth/SET_DATA', (data) => ({ data }));

export const logOut = createAction('@auth/LOG_OUT', () => ({ }));
export const logOutAction = createAction('@auth/LOG_OUT_ACTION', () => ({ }));

export const getNewConfirmationCode = createAction('@auth/GET_CONFIRMATION_CODE', () => ({}));
export const sendConfirmationCode = createAction('@auth/CONFIRMATION_CODE', data => ({data}));

export const setNewPassword = createAction('@auth/SET_NEW_PASSWORD', data => ({data}));

export const lostPassword = createAction('@auth/LOST_PASSWORD', data => ({data}));

export const setSuccess = createAction('@auth/SET_SUCCESS', (success) => ({ success }));
export const setError = createAction('@auth/SET_ERROR', (error) => ({ error }));
export const changeField = createAction('@auth/CHANGE_FIELD', (field, value) => ({ field, value }));
