import { createAction } from 'redux-actions-helpers';

export const getComments = createAction('@comments/GET_COMMENTS', (data) => ({data}));
export const setComments = createAction('@comments/SET_COMMENTS', (data) => ({data}));

export const addComment = createAction('@comments/ADD_COMMENT', (data) => ({data}));
export const addSubComments = createAction('@comments/ADD_SUBCOMMENTS', (data) => ({data}));

export const deleteComment = createAction('@comments/DELETE_COMMENT', (data) => ({data}));
export const deleteSubComment = createAction('@comments/DELETE_SUBCOMMENT', (data) => ({data}));

export const updateComment = createAction('@comments/UPDATE_COMMENT', (data) => ({data}));
export const updateSubComment = createAction('@comments/UPDATE_SUBCOMMENT', (data) => ({data}));