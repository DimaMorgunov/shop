import { createAction } from 'redux-actions-helpers';

export const setMessage = createAction('@socket/SET_CHAT_MESSAGE', (data) => ({ data }));

