import React, {Component} from 'react';
import {View, Text, ScrollView, StyleSheet, TouchableOpacity, TextInput, Alert} from 'react-native';
import {connect} from 'react-redux';
import Image from "react-native-elements/src/image/Image";
import {AirbnbRating} from 'react-native-elements';
import Swiper from 'react-native-swiper';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {addToBin, deleteItem, getStarItem, removeFromBin} from "../../store/actions/item";
import moment from 'moment';
import {
    getComments,
    addSubComments,
    deleteComment,
    updateComment,
    addComment
} from "../../store/actions/comments";
import config from '../../utils/config'

class OneItemComponent extends Component {
    state = {
        inputData: {
            comment: '',
            subComment: ''
        },

        indexComment: '',
        editText: '',
        showEditButton: false,

        indexSubComment: '',
        editSubComment: '',
        showEditSubButton: false,

        indexAddSubComment: '',
        editAddSubComment: '',
        showAddSubButton: false,

        stars: [],
        averageStars: 0
    };

    componentDidMount() {
        this.props.dispatch(getComments(this.props.navigation.state.params.item._id));
        this.renderStars(this.props.navigation.state.params.item.stars)
        console.log(this.props.navigation.state.params.item._id)
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.updateItem !== nextProps.updateItem) {
            this.renderStars(nextProps.updateItem.stars)
        }
        if(this.props.user !== nextProps.user) {
            this.binButton()
        }
    }

    renderStars = (stars) => {
        let sumRating = 0;
        for (let i = 0; i < stars.length; i++) {
            sumRating = stars[i].rating
                }
        sumRating = sumRating / stars.length;
        this.setState({averageStars: sumRating, stars: stars})
    };

    handleAddToBin = () => {
        this.props.dispatch(addToBin({itemId: this.props.navigation.state.params.item._id}))
    };

    handleDeleteComment = (id) => {
        Alert.alert(
            'Delete this comment?',
            '',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
                {text: 'OK', onPress: () => this.props.dispatch(deleteComment({itemId: this.props.navigation.state.params.item._id, id: id}))},
            ],
            {cancelable: false},
        )
    };

    ratingCompleted = (id, rating) => {
        Alert.alert(
            `You want to deliver this product on ${rating}`,
            '',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
                {text: 'OK', onPress: () => this.props.dispatch(getStarItem({id, rating}))},
            ],
            {cancelable: false},
        )
    };

    binButton = () => {
        console.log(Object.assign(this.props.user.bin).indexOf(this.props.navigation.state.params.item._id) >= 0)
        console.log()
        if (Object.assign(this.props.user.itemList).indexOf(this.props.navigation.state.params.item._id) < 0 &&
            Object.assign(this.props.user.bin).indexOf(this.props.navigation.state.params.item._id) < 0) {
            return <TouchableOpacity style={styles.addToBinButton} onPress={this.handleAddToBin}>
                <Text style={styles.addToBinButtonText}>Add to bin</Text>
            </TouchableOpacity>
        } else if (Object.assign(this.props.user.bin).indexOf(this.props.navigation.state.params.item._id) >= 0) {
            return <TouchableOpacity style={styles.addToBinButton} onPress={this.handlerRemoveFromBin}>
                <Text style={styles.addToBinButtonText}>Remove from bin</Text>
            </TouchableOpacity>
        }
    };

    editButton = () => {
        const {item} = this.props.navigation.state.params;
        if (Object.assign(this.props.user.itemList).indexOf(item._id) >= 0 || this.props.user.role === 'admin') {
            return <TouchableOpacity style={styles.editItemButton} onPress={() => this.props.navigation.navigate('EditUserItem', {item})}>
                <Text style={styles.editItemButtonText}>Edit item</Text>
            </TouchableOpacity>
        }
    };

    handlerRemoveFromBin = () => {
        this.props.dispatch(removeFromBin({itemId: this.props.navigation.state.params.item._id}))
    };

    handleDeleteItem = () => (
        Alert.alert(
            'Delete this Item?',
            '',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => this.props.dispatch(deleteItem(this.state.itemData._id))},
            ],
            {cancelable: false},
        )
    );

    deleteCommentButton = (item) => {
        if (item.ownerId == this.props.user._id || this.props.user.role === 'admin') {
            return <TouchableOpacity onPress={() => this.handleDeleteComment(item.id)} style={styles.deleteCommentButtonWrap}>
                        <Image
                            style={styles.deleteCommentButton}
                            source={require('../../images/main/delete.png')}
                        />
                    </TouchableOpacity>
        }
    };

    deleteSubCommentButton = (item) => {
        if (item.ownerId == this.props.user._id || this.props.user.role === 'admin') {
            return <TouchableOpacity
                        onPress={() => this.handleDeleteComment(item.id)}
                        style={styles.deleteSubCommentButtonWrap}
                    >
                        <Image
                            style={styles.deleteCommentButton}
                            source={require('../../images/main/review.png')}
                        />
                    </TouchableOpacity>
        }
    };

    characteristicItem = (item) => (
        <View>
            <Text style={styles.descriptionTitle}>
                {item.title ? item.title : null}
            </Text>
            <Text style={styles.descriptionText}>
                {item.text ? item.text : null}
            </Text>
        </View>
    );

    descriptionItem = (descriptionItem) => (
        <View>
            <Text style={styles.descriptionTitle}>
                {descriptionItem.title ? descriptionItem.title : null}
            </Text>
            <Text style={styles.descriptionText}>
                {descriptionItem.text ? descriptionItem.text : null}
            </Text>
        </View>
    );

    addCommentButton = () => {
        this.props.dispatch(addComment({
            itemId: this.props.navigation.state.params.item._id,
            comment: this.state.inputData.comment,
            email: this.props.user.email
        }));
        this.setState({inputData: {...this.state.inputData, comment: ''}})
    };

    addSubComment = (id) => {
        this.props.dispatch(addSubComments({
            itemId: this.props.navigation.state.params.item._id,
            subComment: this.state.inputData.subComment,
            number: id,
            email: this.props.user.email
        }));
        this.setState({inputData: {...this.state.inputData, subComment: ''}})
    };

    subCommentButton = (item) => {
        if (item.id === this.state.indexAddSubComment) {
            return <TextInput
                        style={styles.subCommentField}
                        placeholder="Type subcomment"
                        value={this.state.inputData.subComment}
                        onChangeText={(value) => this.setState({inputData: {...this.state.inputData, ['subComment']: value}})}
                        returnKeyType="send"
                        onBlur={ () => this.setState({showAddSubButton: false, indexAddSubComment: ''})}
                        ref={(input) => { this.textSubInput = input; }}
                        onSubmitEditing={() => this.addSubComment(item.id)}
                    />
        } else {
           return <TouchableOpacity onPress={() => this.showSubCommentField(item.id)}>
                        <Text style={styles.addSubCommentButton}>Add subcomment</Text>
                  </TouchableOpacity>
        }
    };

    showSubCommentField = (id) => {
        this.setState({indexAddSubComment: id, showAddSubButton: true}, () => this.textSubInput.focus())
    };

    showCommentEditField = (index) => {
        this.setState({indexComment: index, showEditButton: true}, () => this.editTextInput.focus())
    };

    showSubCommentEditField = (index) => {
        this.setState({indexSubComment: index, showEditSubButton: true}, () => this.editTextSubInput.focus())
    };

    editComment = (id) => {
      this.props.dispatch(updateComment({
          editText: this.state.editText,
          itemId: this.props.navigation.state.params.item._id,
          index: id
      }))
        this.setState({showEditButton: false, editText: ''})
    };

    editSubComment = (index) => {
        this.props.dispatch(updateComment({
            editText: this.state.editSubComment,
            itemId: this.props.navigation.state.params.item._id,
            index: index
        }))
        this.setState({showEditButton: false, editText: ''})
    };

    render() {
        const item = this.props.navigation.state.params.item;
        const {inputData, stars, averageStars} = this.state;
        return (
            <View style={{flex: 1}}>
                <Swiper
                    showsPagination={false}
                    showsButtons={false}
                    loop={false}
                >
                    <View style={{flex: 1}}>
                        <Text style={styles.headerTitle}>{item.title ? item.title : 'Product name'}</Text>
                        <Image
                            style={styles.image}
                            resizeMode="cover"
                            source={{uri: `${config.baseUrl}/api/image/?id=${item._id}`}}
                        />
                        <View>
                            {this.binButton()}
                            {this.editButton()}
                        </View>
                        <View style={styles.paramsWrap}>
                            <View style={styles.paramsItem}>
                                <Text style={styles.paramsTitle}>Type: </Text>
                                <Text style={styles.paramsText}>{item.type}</Text>
                            </View>
                            <View style={styles.paramsItem}>
                                <Text style={styles.paramsTitle}>Owner: </Text>
                                <Text style={styles.paramsText}>{item.owner}</Text>
                            </View>
                            <View style={styles.paramsItem}>
                                <Text style={styles.paramsTitle}>Price: </Text>
                                <Text style={styles.paramsText}>{item.price} UAH</Text>
                            </View>
                            <View>
                                <Text style={styles.ratingText}>Total count {averageStars? averageStars : 0}/5 ({stars.length})</Text>
                                <AirbnbRating
                                    fractions={1}
                                    showRating
                                    reviews={[]}
                                    defaultRating={averageStars ? averageStars : 0}
                                    onFinishRating={(rating) => this.ratingCompleted(item._id,rating)}
                                    style={styles.rating}
                                />
                            </View>
                        </View>

                    </View>
                    <ScrollView style={styles.secondTabWrap}>
                        <Text style={styles.secondTabTitle}>
                            Description:
                        </Text>
                        {
                            item.description.length ? item.description.map((descriptionItem) => (
                                    this.descriptionItem(descriptionItem)
                                ))
                                : null
                        }
                        <Text style={styles.secondTabTitle}>
                            Characteristics:
                        </Text>
                        {
                            item.characteristic.length ? item.characteristic.map((item) => (
                                    this.characteristicItem(item)
                                ))
                                : null
                        }
                    </ScrollView>
                    <ScrollView>
                        <View>
                            {
                                this.props.comments.comments.length ? this.props.comments.comments.map((item, index) => (
                                    <View>
                                        <View style={styles.comment}>
                                            {
                                                this.deleteCommentButton(item)
                                            }
                                            <Text style={styles.commentName}>{item.name}</Text>
                                            <Text style={styles.commentDate}>create: {moment(item.createAt).format("MMM Do YY")}</Text>
                                            <Text style={styles.commentText}>{item.text}</Text>
                                            {
                                                item.ownerId === this.props.user._id || this.props.user.role === 'admin' ?
                                                    this.state.indexComment === index && this.state.showEditButton ?
                                                        <View>
                                                            <TextInput
                                                                style={styles.commentField}
                                                                placeholder="EditComment"
                                                                value={this.state.editText}
                                                                onChangeText={(value) => this.setState({editText: value})}
                                                                returnKeyType="send"
                                                                onSubmitEditing={() => this.editComment(item.id)}
                                                                ref={(input) => { this.editTextInput = input; }}
                                                                onBlur={() => this.setState({showEditButton: false, editText: ''})}
                                                            />
                                                        </View>
                                                        :
                                                        <TouchableOpacity onPress={() => this.showCommentEditField(index)}>
                                                            <View>
                                                                <Text style={styles.editButton}>Edit</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                    : null
                                            }
                                            <View style={styles.subComment}>
                                                {
                                                    this.props.comments.comments[index].subComment ?
                                                        this.props.comments.comments[index].subComment.map((subComment, subCommentIndex) =>
                                                            (
                                                                <View>
                                                                    {this.deleteSubCommentButton(subComment)}
                                                                    <Text>{subComment.name}</Text>
                                                                    <Text>create: {moment(subComment.createAt).format("MMM Do YY")}</Text>
                                                                    <Text style={styles.subCommentText}>{subComment.text}</Text>
                                                                    {
                                                                        subComment.ownerId === this.props.user._id || this.props.user.role === 'admin'?
                                                                            this.state.indexSubComment === subComment.id && this.state.showEditSubButton ?
                                                                                <View>
                                                                                    <TextInput
                                                                                        style={styles.commentField}
                                                                                        placeholder="EditComment"
                                                                                        value={this.state.editSubComment}
                                                                                        onChangeText={(value) => this.setState({editSubComment: value})}
                                                                                        returnKeyType="send"
                                                                                        onSubmitEditing={() => this.editSubComment(subComment.id)}
                                                                                        ref={(input) => { this.editTextSubInput = input; }}
                                                                                        onBlur={ () => this.setState({showEditSubButton: false, editSubComment: ''})}
                                                                                    />
                                                                                </View>
                                                                                :
                                                                                <TouchableOpacity onPress={() => this.showSubCommentEditField(subComment.id)}>
                                                                                    <View>
                                                                                        <Text style={styles.editButton}>Edit</Text>
                                                                                    </View>
                                                                                </TouchableOpacity>
                                                                            : null
                                                                    }
                                                                </View>

                                                    ))
                                                    : null
                                                }
                                                {
                                                    this.subCommentButton(item)
                                                }
                                            </View>
                                        </View>
                                        <View>

                                        </View>
                                    </View>
                                )) : null
                            }
                            <TextInput
                                style={[styles.commentField, styles.commentButtonField]}
                                placeholder="Type comment"
                                value={inputData.comment}
                                onChangeText={(value) => this.setState({inputData: {...this.state.inputData, ['comment']: value}})}
                                returnKeyType="send"
                                onSubmitEditing={this.addCommentButton}
                            />
                        </View>
                    </ScrollView>
                </Swiper>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: hp('2%'),
        fontWeight: 'bold',
        textAlign: 'center'
    },
    image: {
        marginVertical: hp('1%'),
        alignSelf: 'center',
        width: wp('80%'),
        height: hp('30%'),
        backgroundColor: "#fff",
        resizeMode: 'contain',
    },
    addToBinButton: {
        width: wp('100%'),
        height: hp('4.5%'),
        backgroundColor: '#e3e3e3',
        paddingTop: hp('1%')
    },
    addToBinButtonText: {
        alignSelf: 'center',
        fontSize: hp('2%')
    },
    editItemButton: {
        width: wp('100%'),
        height: hp('5.5%'),
        backgroundColor: 'yellow',
        paddingTop: hp('1%'),
        marginVertical: hp('1%')
    },
    editItemButtonText: {
        color: 'black',
        alignSelf: 'center',
        fontSize: hp('2%')
    },
    paramsWrap: {
        marginHorizontal: wp('3%'),
        marginVertical: hp('2%')
    },
    paramsItem: {
        flexDirection: 'row',
        paddingVertical: hp('0.75%')
    },
    paramsTitle: {
        fontWeight: 'bold',
        fontSize: hp('2%')
    },
    paramsText: {
        fontSize: hp('2%'),
        marginLeft: wp('2%')
    },
    secondTabTitle: {
        fontWeight: 'bold',
        fontSize: hp('2%'),
        paddingHorizontal: wp('2%')
    },
    bodyContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        width: '100%'
    },
    bodyType: {
        flexDirection: 'row',
        paddingVertical: 5,
        paddingHorizontal: 15
    },
    typeText: {
        fontWeight: 'bold'
    },
    secondTabWrap: {
        marginHorizontal: wp('4%')
    },
    descriptionTitle: {
        fontWeight: 'bold',
        fontSize: wp('4%'),
        paddingVertical: hp('1%'),
    },
    descriptionText: {
        fontSize: wp('3%'),
        paddingVertical: hp('1%'),
    },
    ratingText: {
        textAlign: 'center',
        fontSize: 20,
        marginTop: 20,
        marginBottom: -20,
    },
    rating: {
        paddingTop: 0,
        marginTop: 0,
        alignItems: 'center'
    },
    comment: {
        width: wp('100%'),
        flexDirection: 'column',
        paddingVertical: wp('5%'),
        paddingHorizontal: wp('5%'),
    },
    commentName: {
        fontSize: wp('5%')
    },
    commentDate: {
        fontSize: wp('4%'),
        marginLeft: wp('1%')
    },
    commentText: {
        color: 'black',
        fontSize: wp('4%'),
        paddingVertical: hp('1%')
    },
    commentField: {
        fontSize: wp('3%'),
        width: wp('100%'),
        height: hp('6%'),
        backgroundColor: '#e3e3e3',
        paddingHorizontal: wp('2%')
    },
    subCommentField: {
        width: wp('80%'),
        height: hp('6%'),
        fontSize: wp('3%'),
        backgroundColor: '#e3e3e3',
        paddingHorizontal: wp('2%'),
        marginVertical: hp('0.75%')
    },
    subComment: {
        width: wp('85%'),
        alignSelf: 'flex-end'
    },
    deleteCommentButtonWrap: {
        width: wp('5%'),
        height: wp('5%'),
        position: 'absolute',
        right: wp('5%'),
        top: hp('4.5%'),
        zIndex: 12,
    },
    commentButtonField: {
        marginBottom: hp('6%'),
        height: hp('7%')
    },
    deleteSubCommentButtonWrap: {
        width: wp('5%'),
        height: wp('5%'),
        position: 'absolute',
        right: wp('0.5%'),
        top: hp('1%'),
        zIndex: 12,
    },
    deleteCommentButton: {
        width: wp('5%'),
        height: wp('5%'),
    },
    subCommentText: {
        color: 'black',
        marginVertical: hp('1%')
    },
    editButton: {
        marginVertical: hp('1%'),
        color: 'blue',
        fontSize: wp('3%')
    },
    addSubCommentButton: {
        marginVertical: hp('1%'),
        color: 'blue',
        fontSize: wp('3.5%')
    }
});

export const OneItem = (connect((state) => ({
    user: state.auth.data,
    success: state.auth.success,
    error: state.auth.error,
    comments: state.comments,
    updateItem: state.item.updateItem
}))(OneItemComponent));