import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
    View,
    Text,
    FlatList,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Image,
    KeyboardAvoidingView,
    Keyboard
} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {registerUser} from "../../store/actions/auth";
import ImagePicker from 'react-native-image-picker';
import {ValidationFields} from '../../utils/ValidationFields';



const options = {
    title: 'Select image',
};

class RegistrationComponent extends Component {
    state = {
        data: {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            photo: ''
        },
        errors: {
            first_name: false,
            last_name: false,
            email: false,
            password: false,
            photo: false,
        },
    };



    handlePhoto = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            /*this.handleBase64(response);*/
            if (response.data) {
                const photos = {uri: response.uri, photoBase64: response.data};
                this.setState({data: {...this.state.data, ['photo']: photos}});
            }
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('1')
            }
        });
    };

    /*handleBase64 = (path) => {
        console.log(path);
        NativeModules.RNAssetResizeToBase64.assetToResizedBase64(path.data, 500, 500, (err, base64) => console.log(base64))
    };*/

    handleSubmit = () => {
        const errorField = ValidationFields(this.state.data);
        this.setState({errors: {...this.state.errors, ...errorField.obj}}, () => console.log(this.state));
        if (errorField.arr.length == 0) {
            this.props.dispatch(registerUser({...this.state.data}))
        }
    };

    render() {
        const {data, errors} = this.state;
        return (
            <KeyboardAvoidingView style={styles.container} behavior="position">
                <View>
                    <TouchableOpacity onPress={this.handlePhoto}>
                        <Image
                            style={[styles.image, data.photo.uri ? styles.photo : null, errors.photo ? styles.photoError : null]}
                            source={{uri: data.photo.uri}}
                        />
                    </TouchableOpacity>
                </View>
                <TextInput
                    style={[styles.Input, errors.first_name ? styles.inputError : null]}
                    placeholder="First name"
                    value={data.first_name}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['first_name']: value}})}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                        this.last_name.focus();
                    }}
                />
                <TextInput
                    ref={(input) => {
                        this.last_name = input;
                    }}
                    style={[styles.Input, errors.last_name ? styles.inputError : null]}
                    placeholder="Last name"
                    value={data.last_name}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['last_name']: value}})}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                        this.email.focus();
                    }}
                />
                <TextInput
                    ref={(input) => {
                        this.email = input;
                    }}
                    style={[styles.Input, errors.email ? styles.inputError : null]}
                    placeholder="E-mail"
                    keyboardType="email-address"
                    value={data.email}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['email']: value}})}
                    returnKeyType="next"
                    onSubmitEditing={() => {
                        this.password.focus();
                    }}
                />
                <TextInput
                    ref={(input) => {
                        this.password = input;
                    }}
                    style={[styles.Input, errors.password ? styles.inputError : null]}
                    placeholder="password"
                    secureTextEntry={true}
                    value={data.password}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['password']: value}})}
                    returnKeyType="send"
                    onSubmitEditing={this.handleSubmit}
                />
                <TouchableOpacity onPress={this.handleSubmit} style={styles.buttonWrap}>
                    <Text style={styles.buttonText}>
                        Registration
                    </Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        marginVertical: hp('1%'),
        alignSelf: 'center',
        width: wp('80%'),
        height: hp('35%'),
        backgroundColor: "#e3e3e3",
        resizeMode: 'contain'
    },
    Input: {
        width: wp('80%'),
        height: hp('7%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        paddingHorizontal: wp('2%'),
        marginVertical: hp('1%'),
        alignSelf: 'center',
        fontSize: hp('3%'),
        borderWidth: wp('0.5%'),
        borderColor: '#e6e6e6'
    },
    lostPassword: {
        fontSize: wp('4%'),
        color: 'blue',
        marginHorizontal: wp('11%'),
        marginVertical: hp('2%'),
        alignSelf: 'flex-end'
    },
    buttonWrap: {
        marginTop: hp('2%'),
        alignSelf: 'center',
        width: wp('40%'),
        borderWidth: 1,
        borderColor: 'red',
        paddingVertical: hp('2%'),
        borderRadius: wp('3%')
    },
    buttonText: {
        alignSelf: 'center',
        fontSize: wp('5%'),
        color: 'red'
    },
    errorMessage: {
        alignSelf: 'center',
        color: 'red',
        fontSize: wp('5%'),
        paddingVertical: hp('2%'),
        paddingHorizontal: wp('3%')
    },
    inputError: {
        borderColor: 'red',
    },
    photoError: {
        borderWidth: 5,
        borderColor: 'red',
    },
    photo: {
        backgroundColor: 'white'
    }
});

export const Registration = (connect((state) => ({
    success: state.auth.success,
}))(RegistrationComponent));