import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, TextInput, StyleSheet, TouchableOpacity, Keyboard} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {checkToken, loginUser, authSocial} from "../../store/actions/auth";
import Spinner from 'react-native-loading-spinner-overlay';
import {ValidationFields} from '../../utils/ValidationFields';
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk';

class LoginComponent extends Component {
    state = {
        data:{
            email: '',
            password: '',
        },
        errors: {
            email: '',
            password: ''
        },
    };

    componentDidMount() {
        console.log(LoginManager)
        this.props.dispatch(checkToken())
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.token && this.props.token !== nextProps.token){
            this.props.navigation.navigate('Confirmation');
        }
    }



    handleSubmit = async () => {
        const errorField = ValidationFields(this.state.data);
        console.log(errorField)
        this.setState({errors: {...this.state.errors, ...errorField.obj}});
        if (errorField.arr.length == 0) {
            this.props.dispatch(loginUser({...this.state.data}))
        }
        Keyboard.dismiss();
    };
    render() {
        const {data, errors} = this.state;
        return (
            <View style={styles.container}>
                <TextInput
                    style={[styles.Input, errors.email ? styles.inputError : null]}
                    placeholder="E-mail"
                    keyboardType="email-address"
                    value={data.email}
                    onChangeText={(value) => this.setState({ data: {...this.state.data, ['email']: value} })}
                />
                <TextInput
                    style={[styles.Input, errors.password ? styles.inputError : null]}
                    ref={(input) => { this.secondTextInput = input; }}
                    secureTextEntry={true}
                    placeholder="Password"
                    value={data.password}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['password']: value}})}
                />
                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('LostPassword')}
                >
                    <Text style={styles.lostPassword}>
                        Lost password?
                    </Text>
                </TouchableOpacity>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        onPress={this.handleSubmit}
                        style={styles.buttonWrap}
                    >
                        <Text style={styles.buttonText}>
                            Log in
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Registration')}
                        style={styles.buttonWrap}
                    >
                        <Text style={styles.buttonText}>
                            Registration
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.facebookButton}>
                    <LoginButton
                        readPermissions={["email"]}
                        onLoginFinished={
                            (error, result) => {
                                if (error) {
                                    console.log("login has error: " + result.error);
                                } else if (result.isCancelled) {
                                    console.log("login is cancelled.");
                                } else {
                                    AccessToken.getCurrentAccessToken().then(
                                         (data) => {
                                            console.log(data.accessToken.toString())
                                            fetch(`https://graph.facebook.com/v2.5/me?fields=email,name&access_token=${data.accessToken.toString()}`)
                                                .then((response) => response.json())
                                                .then((responseData) => {
                                                console.log(responseData)
                                                    this.props.dispatch(authSocial({...responseData}))
                                                    LoginManager.logOut(() => {
                                                    })
                                                })
                                                .catch((eror) => {
                                                    console.log(eror)
                                                });
                                        }
                                    )
                                }
                            }
                        }
                        onLogoutFinished={() => console.log("logout.")}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: hp('20%')
    },
    Input: {
        width: wp('80%'),
        height: hp('7%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        paddingHorizontal: wp('2%'),
        marginVertical: hp('2%'),
        alignSelf: 'center',
        fontSize: hp('3%'),
        borderWidth: wp('0.5%'),
        borderColor: '#e6e6e6'
    },
    lostPassword: {
        fontSize: wp('4%'),
        color: 'blue',
        marginHorizontal: wp('11%'),
        marginVertical: hp('2%'),
        alignSelf: 'flex-end'
    },
    buttonContainer: {
        width: wp('95%'),
        marginVertical: hp('5%'),
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    buttonWrap: {
        width: wp('40%'),
        borderWidth: 1,
        borderColor: 'red',
        paddingVertical: hp('2%'),
        borderRadius: wp('3%')
    },
    buttonText: {
        alignSelf: 'center',
        fontSize: wp('5%'),
        color: 'red'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    inputError: {
        borderColor: 'red',
    },
    facebookButton: {
        alignSelf: 'center',
    }
});

export const Login = (connect((state) => ({
    success: state.auth.success,
    error: state.auth.error,
    token: state.auth.token
}))(LoginComponent));