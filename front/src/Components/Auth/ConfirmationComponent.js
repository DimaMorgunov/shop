import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, TextInput, StyleSheet, TouchableOpacity, Keyboard} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Spinner from 'react-native-loading-spinner-overlay';
import {getNewConfirmationCode, sendConfirmationCode} from "../../store/actions/auth";
import CodeInput from 'react-native-confirmation-code-field';


class ConfirmationComponent extends Component {
    state = {
        data: {
            code: '',
        }
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.user && this.props.user !== nextProps.user && nextProps.user.confirmation) {
            this.props.navigation.navigate('Main')
        }
    }

    onFinishCheckingCode = code => {
        this.props.dispatch(sendConfirmationCode(code))
    };

    handleGetNewCode = () => {
        this.setState({errors: {message: ''}});
        this.props.dispatch(getNewConfirmationCode())
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.inputWrap}>
                    <CodeInput
                        autoFocus={true}
                        codeLength={5}
                        inputPosition="center"
                        size={wp('15%')}
                        onFulfill={this.onFinishCheckingCode}
                        containerProps={{size: wp('5%')}}
                        activeColor="#000"
                        inactiveColor="#e8e8e8"
                    />
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        onPress={this.handleGetNewCode}
                        style={styles.buttonWrap}
                    >
                        <Text style={styles.buttonText}>
                            Send new code
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={this.onFinishCheckingCode}
                        style={styles.buttonWrap}
                    >
                        <Text style={styles.buttonText}>
                            Confirmed
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: hp('15%')
    },
    inputWrap: {
        height: hp('10%'),
        width: wp('100%')
    },
    buttonContainer: {
        width: wp('95%'),
        marginVertical: hp('5%'),
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    buttonWrap: {
        width: wp('40%'),
        borderWidth: 1,
        borderColor: 'red',
        paddingVertical: hp('2%'),
        borderRadius: wp('3%')
    },
    buttonText: {
        alignSelf: 'center',
        fontSize: wp('5%'),
        color: 'red'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    inputError: {
        borderColor: 'red',
    }
});

export const Confirmation = (connect((state) => ({
    user: state.auth.data,
    token: state.auth.token
}))(ConfirmationComponent));