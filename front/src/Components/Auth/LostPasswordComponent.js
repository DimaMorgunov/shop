import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, TextInput, StyleSheet, TouchableOpacity, Keyboard} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {lostPassword} from "../../store/actions/auth";
import {ValidationFields} from '../../utils/ValidationFields';


class LostPasswordComponent extends Component {
    state = {
        data:{
            email: ''
        },
        errors: {
            email: ''
        },
    };

    handleSubmit = async () => {
        const errorField = ValidationFields(this.state.data);
        this.setState({errors: {...this.state.errors, ...errorField.obj}});
        if (errorField.arr.length == 0) {
            this.props.dispatch(lostPassword({...this.state.data}))
        }
    };

    render() {
        const {data, errors} = this.state;
        return (
            <View style={styles.container}>
                <TextInput
                    style={[styles.Input, errors.email ? styles.inputError : null]}
                    ref={(input) => { this.secondTextInput = input; }}
                    placeholder="E-mail"
                    value={data.email}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['email']: value}})}
                    returnKeyType = "ok"
                    onSubmitEditing={() => {Keyboard.dismiss();}}
                />
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        onPress={this.handleSubmit}
                        style={styles.buttonWrap}
                    >
                        <Text style={styles.buttonText}>
                            Send new password
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: hp('20%')
    },
    Input: {
        width: wp('80%'),
        height: hp('7%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        paddingHorizontal: wp('2%'),
        marginTop: hp('0.5%'),
        alignSelf: 'center',
        fontSize: hp('3%'),
        borderWidth: wp('0.5%'),
        borderColor: '#e6e6e6'
    },
    lostPassword: {
        fontSize: wp('4%'),
        color: 'blue',
        marginHorizontal: wp('11%'),
        marginVertical: hp('2%'),
        alignSelf: 'flex-end'
    },
    buttonContainer: {
        width: wp('95%'),
        marginVertical: hp('5%'),
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    buttonWrap: {
        width: wp('40%'),
        borderWidth: 1,
        borderColor: 'red',
        paddingVertical: hp('2%'),
        borderRadius: wp('3%'),
        alignSelf: 'center'
    },
    buttonText: {
        alignSelf: 'center',
        fontSize: wp('5%'),
        textAlign: 'center',
        color: 'red'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    inputError: {
        borderColor: 'red',
    }
});

export const LostPassword = (connect((state) => ({
    success: state.auth.success,
    error: state.auth.error,
    token: state.auth.token
}))(LostPasswordComponent));