import React, {Component} from 'react';
import {View, Text, ScrollView, TextInput, StyleSheet, KeyboardAvoidingView, Keyboard} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {MESSAGE_SEND, LOGOUT} from "../../utils/EventsChat";
import {setMessage} from '../../store/actions/socket';
import {HeaderBackButton} from "react-navigation";

class ChatComponent extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerLeft: (<HeaderBackButton onPress={() => {
                navigation.navigate('Main')
            }}/>)
        }
    };

    state = {
        socket: null,
        user: null,
        inputText: '',
        inputPosition: hp('0')
    };

    componentDidMount() {
        const {data, registerHandler} = this.props.navigation.state.params;
        registerHandler(this.onMessageReceived);
        this.setState({...data});
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow,
        );
    }

    _keyboardDidShow = () => {
        this.setState({inputPosition: hp('17%')})
    };

    onMessageReceived = (entry) => {
        console.log('onMessageReceived:', entry)
        this.updateChatHistory(entry)
    };

    updateChatHistory = (messages) => {
        if(messages.hasOwnProperty('messages')) {
            this.props.dispatch(setMessage(messages.messages));
        } else {
            this.props.dispatch(setMessage(messages));
        }

    };

    sendMessage = (activeRoom, message) => {
        const data = {text: message, user: this.state.user};
        const {sendMessage} = this.props.navigation.state.params;
        sendMessage({activeRoom, data})
    };

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        const {leave} = this.props.navigation.state.params;
        leave(LOGOUT);
    }

    render() {
        const {activeRoom, inputText, inputPosition} = this.state;
        const {message, user} = this.props;
        return (
            <KeyboardAvoidingView style={{flex: 1}} behavior="position">
                <ScrollView style={styles.chatWrap}>
                    {
                        message.length ? message.map((item, index) => (
                            <View
                                style={[styles.messageWrap, item.user.email === user.email ? styles.ownerMessage : null]}>
                                <Text style={styles.messageName}>{item.user.name}</Text>
                                <Text style={styles.messageText}>{item.text}</Text>
                            </View>
                        )) : null
                    }
                </ScrollView>
                <View style={{paddingBottom: hp('5%'), marginBottom: wp('5%')}}>
                    <TextInput
                        style={[styles.Input, inputPosition ? {bottom: inputPosition} : null]}
                        value={inputText}
                        autoCorrect={false}
                        onChangeText={(value) => this.setState({inputText: value})}
                        returnKeyType={"send"}
                        onBlur={() => this.setState({inputPosition: 0})}
                        onSubmitEditing={() => {
                            this.message.blur();
                            this.sendMessage(activeRoom, inputText)
                        }}
                        ref={(input) => {
                            this.message = input;
                        }}
                    />
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: hp('20%')
    },
    chatWrap: {
        width: wp('100%'),
        height: hp('80%')
    },
    messageWrap: {
        width: wp('60%'),
        paddingHorizontal: wp('2.5%'),
        paddingVertical: hp('1%'),
        marginHorizontal: wp('2.5%'),
        marginVertical: hp('1%'),
        borderWidth: 2,
        borderRadius: wp('2%'),
        borderColor: '#e2e2e2'
    },
    ownerMessage: {
        backgroundColor: '#a5cce5',
        alignSelf: 'flex-end'
    },
    messageName: {
        fontSize: wp('3%')
    },
    messageText: {
        fontSize: wp('3.5%')
    },
    Input: {
        position: 'absolute',
        width: wp('100%'),
        height: hp('7%'),
        backgroundColor: '#e6e6e6',
        /*paddingHorizontal: wp('2%'),
        marginBottom: hp('5%'),*/
        bottom: 5,
        alignSelf: 'center',
        fontSize: wp('4%'),
        borderWidth: wp('0.5%'),
        borderColor: '#e6e6e6'
    }
});

export const Chat = (connect((state) => ({
    user: state.auth.data,
    message: state.socket.message,
    success: state.auth.success,
    error: state.auth.error
}))(ChatComponent));