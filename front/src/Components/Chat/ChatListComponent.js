import React, {Component} from 'react';
import {View, Text, ScrollView, TextInput, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import config, {chatPorts} from '../../utils/config';
import io from 'socket.io-client';
import {MESSAGE_SEND, MESSAGE_RECIEVED, CREATE_CHAT, LOGOUT} from '../../utils/EventsChat'
import {setMessage} from '../../store/actions/socket';
import socket from "../../store/reducers/socket";

/*const socket = io.connect(config.baseUrl);*/


class ChatListComponent extends Component {
    static navigationOptions = {
        title: 'Chat list',
    };

    state = {
        socket: null,
        user: null,
        activeRoom: null
    };

    componentDidMount() {

        this.subs = [
            this.props.navigation.addListener('willFocus', this.componentWillFocus)
        ]
    }

    componentWillFocus = () => {
        this.initSocket()
    };

    initSocket = () => {
        console.log(this.state.socket)
        const socket = function () {
            const socket = io.connect(config.baseUrl, {'forceNew': true});

            function join(event, chatroomName) {
                console.log(socket)
                socket.emit(event, chatroomName)
            }

            function leave(chatroomName, cb) {
                console.log('logout')
                console.log(socket)
                socket.emit(LOGOUT, chatroomName, cb)
            }

            function registerHandler(onMessageReceived) {
                socket.on(MESSAGE_SEND, onMessageReceived)
                console.log(onMessageReceived)
            }

            function message(obj) {
                socket.emit(MESSAGE_SEND, {...obj})
            }

            return {
                join,
                leave,
                registerHandler,
                message,
            }
        };
        const user = {email: this.props.user.email, name: `${this.props.user.first_name} ${this.props.user.last_name}`};
        this.setState({user, socket: socket()}, () =>  console.log(this.state.socket, 123));
    };

    sendMessage = (obj) => {
        this.state.socket.message(obj)
    };

    componentWillUnmount() {
        this.subs.forEach(sub => sub.remove())
    }

    connectToCurrentChat = (room) => {
        const {user} = this.state;

        this.state.socket.join(CREATE_CHAT, {room: room});

        this.props.navigation.navigate('Chat', {
            registerHandler: this.state.socket.registerHandler,
            sendMessage: this.sendMessage,
            leave: this.state.socket.leave,
            data: {
                activeRoom: room,
                user: user,
            }
        })
    };


    render() {

        const {message, activeRoom} = this.state;

        return (
            <ScrollView style={styles.container}>
                {
                    chatPorts.map((item, index) => (
                        <TouchableOpacity onPress={() => this.connectToCurrentChat(item)}>
                            <View style={styles.itemContainer}>
                                <Text style={styles.itemTitle}>
                                    {item}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    ))
                }
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemContainer: {
        width: wp('100%'),
        height: hp('8.5%'),
        borderWidth: 1,
        borderColor: '#e6e6e6'
    },
    itemTitle: {
        flex: 1,
        paddingLeft: wp('10%'),
        paddingTop: hp('3%'),
        fontSize: wp('4%')
    }
});

export const ChatList = (connect((state) => ({
    user: state.auth.data,
    success: state.auth.success,
    error: state.auth.error
}))(ChatListComponent));