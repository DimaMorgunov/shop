import React, {Component} from 'react';
import {View, Text, ScrollView, FlatList, StyleSheet} from 'react-native';
import { connect } from 'react-redux';
import {getBinItems} from '../store/actions/item';
import {ListItem} from 'react-native-elements'
import config from '../utils/config'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


class BasketComponent extends Component {
    static navigationOptions = {
        title: 'Basket',
    };

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener('willFocus', this.componentWillFocus)
        ]
    }

    componentWillFocus = () => {
        this.props.dispatch(getBinItems(this.props.user.bin))
    };


    renderItem = ({item}) => (
        <ListItem
            title={item.title}
            titleStyle={styles.itemStyle}
            rightTitle={`${item.price} UAH`}
            rightTitleStyle={styles.itemRightTitle}
            subtitle={`Type: ${item.type}`}
            subtitleStyle={styles.itemSubtitle}
            rightSubtitle={`Owner: ${item.owner}`}
            rightSubtitleStyle={styles.itemRightSubtitle}
            leftAvatar={{source: {uri: `${config.baseUrl}/api/image/?id=${item._id}`}}}
            onPress={() => this.props.navigation.navigate('OneItem', {item})}
            key={item._id}
        />
    );

    render() {
        return (
            <ScrollView>
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.bin.length ? this.props.bin : null}
                    renderItem={this.renderItem}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    searchWrap: {
        flexDirection: 'row',
        width: wp('100%'),
        paddingTop: hp('2%'),
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    nameSearchInput: {
        width: wp('80%'),
        height: hp('6%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        paddingHorizontal: wp('2%'),
        fontSize: wp('5%')
    },
    clearButton: {
        position: 'absolute',
        width: wp('5%'),
        height: wp('5%'),
        left: wp('73%'),
        top: hp('3.5%'),
    },
    searchButton: {
        width: wp('15%'),
        fontSize: wp('4%'),
        color: 'blue'
    },
    searchBarWrap: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 10,
        paddingHorizontal: 0
    },
    searchBarItem: {
        textAlign: 'center',
        width: 90,
        borderRadius: 18,
        backgroundColor: '#e6e6e6',
        height: 25,
        paddingHorizontal: 5,
        fontSize: 12,
    },
    searchBarPicker: {
        backgroundColor: '#e6e6e6',
        height: hp('4.5%'),
        width: wp('30%'),
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        borderRadius: wp('3%'),
    },
    itemStyle: {
        fontSize: wp('3.5%')
    },
    itemRightTitle: {
        fontSize: wp('3%')
    },
    itemSubtitle: {
        fontSize: wp('3%')
    },
    itemRightSubtitle: {
        fontSize: wp('3%')
    }
});

export const Basket = (connect((state) => ({
    user: state.auth.data,
    bin: state.item.bin
}))(BasketComponent));