import React, {Component} from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    TextInput,
    Dimensions,
    Alert,
    TouchableWithoutFeedback,
    Platform
} from 'react-native';
import Image from "react-native-elements/src/image/Image";
import {connect} from 'react-redux';
import {Picker} from 'native-base';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import config from '../../utils/config';
import {deleteItem, updateItem} from "../../store/actions/item";
import {ValidationFields} from "../../utils/ValidationFields";

const options = {
    title: 'Select image',
};

class EditUserItemComponent extends Component {

    state = {
        itemData: {
            title: '',
            price: '',
            type: '',
            characteristic: [],
            description: [],
            photo: {uri: '', photoBase64: ''},
            owner: `${this.props.user.first_name} ${this.props.user.last_name}`
        },
        inputError: {
            title: '',
            price: '',
            type: '',
        },
        characteristicTitle: '',
        characteristicText: '',
        descriptionTitle: '',
        descriptionText: ''
    };

    componentDidMount() {
        this.setState({itemData: {...this.state.itemData, ...this.props.navigation.state.params.item}})
    }

    componentWillReceiveProps(nextProps){
        if (this.props.success !== nextProps.success && nextProps.success) {
            this.props.navigation.navigate("ProfileItemList")
            console.log('edit cwrp')
        }
    }

    handlePhoto = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            const photos = {uri: response.uri, photoBase64: response.data}
            this.setState({itemData: {...this.state.itemData, ['photo']: photos}});
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('1')
            }
        });
    };

    handleInput = (name, value) => {
        this.setState({itemData: {...this.state.itemData, [name]: value}})
    };

    handleOnChange = (name, value) => {
        this.setState({itemData: {...this.state.itemData, [name]: value}});
    };

    addNewCharacteristic = (field) => {
        if(field === 'characteristic' || field === 'description') {
            let array = this.state.itemData[field];
            array.push({title: this.state.characteristicTitle, text: this.state.characteristicText})
            this.setState({itemData: {...this.state.itemData, [field]: array}});
        }
    };

    deleteCurrentItem = (field, number) => (
        Alert.alert(
            'Delete this Item?',
            '',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel',},
                {text: 'OK', onPress: () => this.deleteArrayItem(field, number)},
            ],
            {cancelable: false},
        )
    );

    deleteArrayItem = (field, number) => {
        if (field === 'characteristic' || field === 'description') {
            let array = this.state.itemData[field];
            array = array.filter((item, index) => index !== number);
            this.setState({itemData: {...this.state.itemData, [field]: array}});
        }
    };

    deleteUserItem = () => (
        Alert.alert(
            'Delete this Item?',
            '',
            [
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                {text: 'OK', onPress: () => this.props.dispatch(deleteItem(this.state.itemData._id))},
            ],
            {cancelable: false},
        )
    );

    handleSubmit = () => {
        const errorField = ValidationFields(this.state.itemData);
        console.log(errorField)
        this.setState({inputError: {...this.state.inputError, ...errorField.obj}}, () => console.log(this.state));
        if (errorField.arr.length == 0) {
            this.props.dispatch(updateItem(this.state.itemData))
        }
    };

    render() {
        console.log('EditUserProfile')
        const {itemData, inputError, characteristicTitle, characteristicText, descriptionTitle, descriptionText} = this.state;
        const {item} = this.props.navigation.state.params;
        return (
            <ScrollView style={styles.container}>
                <View>
                    <TouchableOpacity onPress={this.handlePhoto}>
                        <Image
                            style={[styles.image, itemData.photo.uri ? styles.photo : null]}
                            source={{
                                uri:
                                    itemData.photo.uri ?
                                        itemData.photo.uri
                                        : `${config.baseUrl}/api/image?id=${item._id}`
                            }}
                        />
                    </TouchableOpacity>
                </View>
                <View>
                    <Text style={styles.title}>Product title:</Text>
                    <TextInput
                        placeholder="Title"
                        style={[styles.input, inputError.title ? styles.inputError : null]}
                        value={itemData.title}
                        onChangeText={(value) => this.handleInput('title', value)}
                        returnKeyType="next"

                    />
                    <Text style={styles.title}>Price:</Text>
                    <TextInput
                        placeholder="Price"
                        style={[styles.input, inputError.price ? styles.inputError : null]}
                        keyboardType = 'numeric'
                        value={itemData.price}
                        onValueChange={(value) => this.handleOnChange('price', value)}
                    />
                    <Text style={styles.title}>Product type:</Text>
                    <Picker
                        placeholder="Type"
                        selectedValue={itemData.type}
                        onValueChange={(value) => this.handleOnChange('type', value)}
                        style={styles.picker}
                    >
                        <Picker.Item label="Pleace choose" value=""/>
                        <Picker.Item label="Phone" value="phone"/>
                        <Picker.Item label="Laptop" value="laptop"/>
                        <Picker.Item label="Monoblock" value="monoblock"/>
                        <Picker.Item label="Monitor" value="monitor"/>
                        <Picker.Item label="Accessories" value="accessories"/>
                        <Picker.Item label="Other" value="other"/>
                    </Picker>
                </View>
                <View>
                    <Text style={styles.title}>Characteristics:</Text>
                    {
                        itemData.characteristic.length ?
                            itemData.characteristic.map((item, number) => (
                                <TouchableWithoutFeedback
                                    onPress={() => this.deleteCurrentItem('characteristic', number)}>
                                    <View style={styles.characteristicItem}>
                                        <Image
                                            style={styles.deleteItemItem}
                                            source={require('../../images/main/delete.png')}
                                        />

                                        <Text style={styles.characteristicItemTitle}>{item.title}</Text>
                                        <Text style={styles.characteristicItemText}>{item.text}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            ))
                            : null
                    }
                    <Text style={styles.subTitle}>Characteristic title:</Text>
                    <TextInput
                        placeholder="Title"
                        style={styles.input}
                        value={characteristicTitle}
                        onChangeText={(value) => this.setState({characteristicTitle: value})}
                        returnKeyType='ok'
                    />
                    <Text style={styles.subTitle}>Characteristic value:</Text>
                    <TextInput
                        placeholder="Text"
                        style={styles.input}
                        value={characteristicText}
                        onChangeText={(value) => this.setState({characteristicText: value})}
                        returnKeyType='ok'
                    />
                    <TouchableOpacity style={styles.addButton} onPress={() => this.addNewCharacteristic('characteristic')}>
                        <Text>Add characteristic</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Text style={styles.title}>Description:</Text>
                    {
                        itemData.description.length ?
                            itemData.description.map((item, number) => (
                                <TouchableWithoutFeedback onPress={() => this.deleteCurrentItem('description', number)}>
                                    <View style={styles.characteristicItem}>
                                        <Image
                                            style={styles.deleteItemItem}
                                            source={require('../../images/main/delete.png')}
                                        />

                                        <Text style={styles.characteristicItemTitle}>{item.title}</Text>
                                        <Text style={styles.characteristicItemText}>{item.text}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            ))
                            : null
                    }
                    <Text style={styles.subTitle}>Description title:</Text>
                    <TextInput
                        placeholder="Title"
                        style={styles.input}
                        value={descriptionTitle}
                        onChangeText={(value) => this.setState({descriptionTitle: value})}
                        returnKeyType='ok'
                    />
                    <Text style={styles.subTitle}>Description value:</Text>
                    <TextInput
                        placeholder="Text"
                        style={styles.input}
                        value={descriptionText}
                        onChangeText={(value) => this.setState({descriptionText: value})}
                        returnKeyType='ok'
                    />
                    <TouchableOpacity style={styles.addButton} onPress={() => this.addNewCharacteristic('description')}>
                        <Text>Add description</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={styles.createButton} onPress={this.handleSubmit}>
                        <Text style={styles.createButtonText}>Update</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={[styles.createButton, {backgroundColor: 'red', marginTop: 0}]}
                                      onPress={this.deleteUserItem}>
                        <Text style={styles.createButtonText}>Delete</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        marginVertical: hp('1%'),
        alignSelf: 'center',
        width: wp('80%'),
        height: hp('35%'),
        backgroundColor: "#e3e3e3",
        resizeMode: 'contain'
    },
    title: {
        fontSize: hp('3%'),
        marginHorizontal: wp('2%'),
    },
    input: {
        width: wp('90%'),
        height: Platform.OS === 'android' ? hp('8%') : hp('5%'),
        fontSize: hp('2%'),
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#eee',
        paddingHorizontal: wp('5%'),
        marginVertical: hp('1%')
    },
    picker: {
        alignSelf: 'center',
        width: wp('90%'),
        height: hp('5%'),
        paddingHorizontal: wp('5%'),
        marginVertical: hp('1%'),
        borderRadius: wp('5%'),
        backgroundColor: '#e6e6e6',
    },
    subTitle: {
        fontSize: hp('2%'),
        marginHorizontal: wp('1.5%'),
    },
    addButton: {
        alignSelf: 'center',
        alignItems: 'center',
        width: wp('100%'),
        height: hp('5%'),
        fontSize: hp('2%'),
        paddingTop: hp('1%'),
        borderRadius: wp('1%'),
        backgroundColor: '#eee',
        marginVertical: hp('1%'),
    },
    characteristicItem: {
        marginHorizontal: wp('6%'),
        marginVertical: hp('1.5%'),
    },
    characteristicItemTitle: {
        fontSize: hp('2%'),
    },
    characteristicItemText: {
        fontSize: hp('1.5%'),
    },
    deleteItemItem: {
        position: 'absolute',
        width: wp('5%'),
        height: wp('5%'),
        left: wp('83%'),
        top: hp('0.5%'),
    },
    createButton: {
        alignItems: 'center',
        paddingTop: hp('1.5%'),
        width: wp('100%'),
        height: hp('6%'),
        backgroundColor: 'blue',
        marginVertical: hp('3%')
    },
    createButtonText: {
        fontSize: hp('3%'),
        color: 'white'
    },
    photo: {
        backgroundColor: 'white'
    },
    inputError: {
        borderColor: 'red',
        borderWidth: 2,
    },
});


export const EditUserItem = (connect((state) => ({
    success: state.auth.success,
    error: state.auth.error,
    user: state.auth.data
}))(EditUserItemComponent));