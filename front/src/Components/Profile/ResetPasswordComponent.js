import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, TextInput, StyleSheet, TouchableOpacity, Keyboard, Alert, Platform, KeyboardAvoidingView} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {setNewPassword} from "../../store/actions/auth";
import {ValidationFields} from '../../utils/ValidationFields';
import {HeaderBackButton} from "react-navigation";



class ResetPasswordComponent extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Reset password',
            headerLeft: (<HeaderBackButton onPress={() => {
                navigation.navigate('Profile')
            }}/>)
        }
    };

    state = {
        data: {
            oldPassword: '',
            newPassword: '',
            repeatNewPassword: '',
        }
    };

    handleSubmit = async () => {
        const errorField = ValidationFields(this.state.data);
        this.setState({errors: {...this.state.errors, ...errorField}});
        Keyboard.dismiss();
        this.props.dispatch(setNewPassword({...this.state.data}))
    };

    render() {
        const {data, errors} = this.state;
        return (
            <View style={styles.container}>
            <KeyboardAvoidingView style={{flex: 1}} behavior="position">
                <TextInput
                    style={[styles.Input]}
                    placeholder="Old password"
                    value={data.oldPassword}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['oldPassword']: value}})}
                />
                <TextInput
                    style={[styles.Input]}
                    ref={(input) => {
                        this.secondTextInput = input;
                    }}
                    secureTextEntry={true}
                    placeholder="New password"
                    value={data.newPassword}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['newPassword']: value}})}
                />
                <TextInput
                    style={[styles.Input]}
                    ref={(input) => {
                        this.secondTextInput = input;
                    }}
                    secureTextEntry={true}
                    placeholder="Repeat new password"
                    value={data.repeatNewPassword}
                    onChangeText={(value) => this.setState({data: {...this.state.data, ['repeatNewPassword']: value}})}
                />
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        onPress={this.handleSubmit}
                        style={styles.buttonWrap}
                    >
                        <Text style={styles.buttonText}>
                            Reset
                        </Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: hp('20%')
    },
    Input: {
        width: wp('80%'),
        height: Platform.OS === 'android' ? hp('8%') : hp('6%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        paddingHorizontal: wp('2%'),
        marginVertical: hp('1%'),
        alignSelf: 'center',
        fontSize: wp('5%'),
        borderWidth: wp('0.5%'),
        borderColor: '#e6e6e6'
    },
    lostPassword: {
        fontSize: wp('4%'),
        color: 'blue',
        marginHorizontal: wp('11%'),
        marginVertical: hp('2%'),
        alignSelf: 'flex-end'
    },
    buttonContainer: {
        width: wp('95%'),
        marginVertical: hp('5%'),
        alignSelf: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    buttonWrap: {
        width: wp('40%'),
        borderWidth: 1,
        borderColor: 'red',
        paddingVertical: hp('2%'),
        borderRadius: wp('3%')
    },
    buttonText: {
        alignSelf: 'center',
        fontSize: wp('5%'),
        color: 'red'
    },
    inputError: {
        borderColor: 'red',
    }
});

export const ResetPassword = (connect((state) => ({
    token: state.auth.token
}))(ResetPasswordComponent));