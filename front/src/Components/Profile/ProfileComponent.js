import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, TextInput, Platform, KeyboardAvoidingView} from 'react-native';
import {connect} from 'react-redux';
import Image from "react-native-elements/src/image/Image";
import config from '../../utils/config';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {updateUser} from '../../store/actions/auth';
import {ValidationFields} from "../../utils/ValidationFields";

const options = {
    title: 'Select image',
};

class ProfileComponent extends Component {
    static navigationOptions = {
        title: 'Profile',
    };

    state = {
        data: {
            first_name: '',
            last_name: '',
            photo: '',
        },
        inputError: {
            first_name: false,
            last_name: false,
            photo: false,
        }
    };

    componentDidMount() {
        const {first_name, last_name} = this.props.user;
        const data = {first_name, last_name};
        this.setState({data: {...this.state.data, ...data}})
    }

    handlePhoto = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            const photos = {uri: response.uri, photoBase64: response.data}
            this.setState({data: {...this.state.data, ['photo']: photos}});
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('1')
            }
        });
    };

    handleSubmit = () => {
        const errorField = ValidationFields(this.state.data);
        this.setState({inputError: {...this.state.inputError, ...errorField.obj}});
        console.log(errorField)
        if (errorField.arr.length >= 1) {
            this.props.dispatch(updateUser({...this.state.data}))
        }
    };

    render() {
        const {data, inputError} = this.state;
        const {user} = this.props;
        return (
            <KeyboardAvoidingView style={styles.container} behavior="position">
                <View>
                    <TouchableOpacity onPress={() => this.handlePhoto()}>
                        <Image
                            style={[styles.image]}
                            source={{ uri: this.state.data.photo.uri ? this.state.data.photo.uri : `${config.baseUrl}/api/image?id=${user._id}`}}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        E-mail:
                    </Text>
                    {user.email ?
                        <Text style={styles.itemText}>
                            {user.email}
                        </Text>
                        : null
                    }
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        Name:
                    </Text>
                    <TextInput
                        style={[styles.input, inputError.first_name ? styles.inputError : null]}
                        placeholder="Name"
                        value={data.first_name}
                        onChangeText={(value) => this.setState({data: {...this.state.data, ['first_name']: value}})}
                    />
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        Surname:
                    </Text>
                    <TextInput
                        style={[styles.input, inputError.last_name ? styles.inputError : null]}
                        placeholder="Surname"
                        value={data.last_name}
                        onChangeText={(value) => this.setState({data: {...this.state.data, ['last_name']: value}})}
                    />
                </View>
                <View style={styles.buttonWrap}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileItemList')}>
                        <View style={styles.itemListWrap}>
                            <Text style={styles.itemTitle}>
                                Your Items :
                            </Text>
                            {user.itemList.length ?
                                <Text style={styles.itemListCount}>
                                    {user.itemList.length}
                                </Text>
                                : null
                            }
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ResetPassword')}>
                        <View style={styles.itemListWrap}>
                            <Text style={styles.itemTitle}>
                                Reset password
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.button}>
                    <TouchableOpacity onPress={this.handleSubmit}>
                        <Text style={styles.buttonText}>Update profile</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        marginVertical: hp('1%'),
        alignSelf: 'center',
        width: wp('80%'),
        height: Platform.OS === 'android' ? hp('30%') : hp('35%'),
        backgroundColor: "#e3e3e3",
        resizeMode: 'contain'
    },
    itemWrap: {
        marginVertical: hp('1%'),
        marginHorizontal: wp('3%'),
        flexDirection: 'row'
    },
    itemTitle: {
        fontSize: wp('5%'),
        fontWeight: 'bold'
    },
    itemText: {
        fontSize: wp('5%'),
        marginLeft: wp('2%')
    },
    input: {
        width: wp('65%'),
        height: Platform.OS === 'android' ? hp('5.5%') : hp('4%'),
        paddingHorizontal: Platform.OS === 'android' ? hp('1%') : hp('2%'),
        borderRadius: wp('3%'),
        fontSize: Platform.OS === 'android' ? hp('2%') : hp('3%'),
        backgroundColor: '#e6e6e6',
        marginLeft: wp('2%')
    },
    buttonWrap: {
        marginVertical: hp('3%'),
    },
    itemListWrap: {
        width: wp('100%'),
        height: hp('8%'),
        marginVertical: hp('0.5%'),
        paddingHorizontal: wp('3%'),
        flexDirection: 'row',
        borderColor: '#e6e6e6',
        borderWidth: 1,
        alignSelf: 'center',
        alignItems: 'center'
    },
    itemListCount: {
        justifyContent: 'flex-end',
        marginLeft: 'auto'
    },
    button: {
        width: wp('50%'),
        height: hp('7%'),
        borderWidth: 1,
        borderRadius: wp('3%'),
        borderColor: 'red',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: wp('4%'),
        fontWeight: 'bold',
        alignSelf: 'center',
        color: '#5a5355'
    },
    photo: {
        backgroundColor: 'white'
    },
    inputError: {
        borderColor: 'red',
        borderWidth: 2,
    },
});

export const Profile = (connect((state) => ({
    success: state.auth.success,
    error: state.auth.error,
    user: state.auth.data,
}))(ProfileComponent));