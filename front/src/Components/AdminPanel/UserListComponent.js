import React, {Component} from 'react';
import {View, Text, ScrollView, FlatList, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {ListItem} from 'react-native-elements'
import config from '../../utils/config'
import {getUsers} from '../../store/actions/auth'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import moment from "moment";
import {Picker} from 'native-base';
import {sortUser} from "../../utils/userSort";

class UserListComponent extends Component {
    static navigationOptions = {
        title: 'UserLisst',
    };

    state = {
        email: '',
        updateAt: '',
        createAt: '',
        sort: '',
        sortedList: [],
        refreshing: false
    };

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener('willFocus', this.componentWillFocus)
        ]
    }

    componentWillFocus = () => {
        this.props.dispatch(getUsers())
    };


    handleSort = () => {
        if (this.state.sort.length !== 0) {
            const array = [...this.props.userList];
            const sortedList = sortUser(this.state.sort, array);
            this.setState({sortedList: sortedList}, () => this.handleRefresh())
        } else {
            this.setState({sort: '', sortedList: []}, () => this.handleRefresh())
        }
    };

    handleRefresh = () => {
        this.setState({refreshing: true,}, () => this.getAllItemsRequest())
    };

    getAllItemsRequest = () => {
        this.setState({refreshing: false}, () => console.log(this.state, this.props.userList))
    };


    renderItem = ({item}) => (
        <ListItem
            title={item.email}
            titleStyle={styles.itemStyle}
            subtitle={`${item.first_name} ${item.last_name}`}
            subtitleStyle={styles.itemSubtitle}
            rightTitle={`Update at: ${moment(item.updatedAt).format('MMMM Do YYYY, h:mm')}`}
            rightTitleStyle={styles.itemRightSubtitle}
            rightSubtitle={`Create at: ${moment(item.createdAt).format('MMMM Do YYYY, h:mm')}`}
            rightSubtitleStyle={styles.itemRightSubtitle}
            leftAvatar={{source: {uri: `${config.baseUrl}/api/image/?id=${item._id}`}}}
            onPress={() => this.props.navigation.navigate('AdminOneUser', {item})}
            key={item._id}
        />
    );

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.sortContainer}>
                    <Picker
                        placeholder="Type"
                        selectedValue={this.state.sort}
                        onValueChange={(value) => this.setState({sort: value})}
                        textStyle={{textAlign: 'left', fontSize: 12, alignItems: 'flex-start'}}
                        style={styles.searchBarPicker}
                    >
                        <Picker.Item label="All" value=""/>
                        <Picker.Item label="email A-Z" value="emailAZ"/>
                        <Picker.Item label="email Z-A" value="emailZA"/>
                        <Picker.Item label="Create at" value="create10"/>
                        <Picker.Item label="Create to" value="create01"/>
                        <Picker.Item label="Update at" value="update10"/>
                        <Picker.Item label="Update to" value="update01"/>
                    </Picker>
                    <View>
                        <TouchableOpacity onPress={this.handleSort}>
                            <Text style={styles.searchButton}>Sort</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.sortedList.length ? this.state.sortedList : this.props.userList}
                    extraData={this.state.refresh}
                    renderItem={this.renderItem}
                    refreshing={this.state.refreshing}
                    onRefresh={this.handleRefresh}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    sortContainer: {
        flexDirection: 'row',
        width: wp('80%'),
        paddingTop: hp('2%'),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    nameSearchInput: {
        width: wp('80%'),
        height: hp('6%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        paddingHorizontal: wp('2%'),
        fontSize: wp('5%')
    },
    searchButton: {
        width: wp('15%'),
        fontSize: wp('4%'),
        marginLeft: wp('5%'),
        color: 'blue'
    },
    searchBarWrap: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 10,
        paddingHorizontal: 0
    },
    searchBarItem: {
        textAlign: 'center',
        width: 90,
        borderRadius: 18,
        backgroundColor: '#e6e6e6',
        height: 25,
        paddingHorizontal: 5,
        fontSize: 12,
    },
    searchBarPicker: {
        backgroundColor: '#e6e6e6',
        height: hp('4.5%'),
        width: wp('10%'),
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        borderRadius: wp('3%'),
    },
    itemStyle: {
        fontSize: wp('3.5%')
    },
    itemRightTitle: {
        fontSize: wp('3%')
    },
    itemSubtitle: {
        fontSize: wp('3%')
    },
    itemRightSubtitle: {
        fontSize: wp('3%')
    }
});

export const UserList = (connect((state) => ({
    userList: state.auth.userList
}))(UserListComponent));