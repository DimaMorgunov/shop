import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, TextInput, Switch} from 'react-native';
import {connect} from 'react-redux';
import Image from "react-native-elements/src/image/Image";
import config from '../../utils/config';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {editUserAdmin} from '../../store/actions/auth';
import {Picker} from 'native-base';

const options = {
    title: 'Select image',
};

class AdminOneUserComponent extends Component {
    static navigationOptions = {
        title: 'Profile',
    };

    state = {
        data: {
            first_name: '',
            last_name: '',
            photo: '',
            blocked: false,
            role: ''
        },
        errors: {
            first_name: false,
            last_name: false,
        }
    };

    componentDidMount() {
        const {first_name, last_name, blocked, role} = this.props.navigation.state.params.item;
        const data = {first_name, last_name, blocked, role};
        this.setState({data: {...this.state.data, ...data}})
    }

    handlePhoto = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            const photos = {uri: response.uri, photoBase64: response.data}
            this.setState({data: {...this.state.data, ['photo']: photos}});
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('1')
            }
        });
    };

    handleSubmit = () => {
        console.log(this.state);
        this.props.dispatch(editUserAdmin({...this.state.data}))
    };

    render() {
        const {data, errors} = this.state;
        const {item} = this.props.navigation.state.params;
        return (
            <View style={styles.container}>
                <View>
                    <TouchableOpacity onPress={() => this.handlePhoto()}>
                        <Image
                            style={[styles.image]}
                            source={{ uri: this.state.data.photo.uri ? this.state.data.photo.uri : `${config.baseUrl}/api/image?id=${item._id}`}}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        E-mail:
                    </Text>
                    {item.email ?
                        <Text style={styles.itemText}>
                            {item.email}
                        </Text>
                        : null
                    }
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        Name:
                    </Text>
                    <TextInput
                        style={[styles.input, errors.first_name ? styles.inputError : null]}
                        placeholder="Name"
                        value={data.first_name}
                        onChangeText={(value) => this.setState({data: {...this.state.data, ['first_name']: value}})}
                    />
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        Surname:
                    </Text>
                    <TextInput
                        style={[styles.input, errors.last_name ? styles.inputError : null]}
                        placeholder="Surname"
                        value={data.last_name}
                        onChangeText={(value) => this.setState({data: {...this.state.data, ['last_name']: value}})}
                    />
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        Blocked:
                    </Text>
                    <Switch
                        style={styles.switch}
                        onValueChange={() => this.setState({data: {...this.state.data, ['blocked']: !data.blocked}})}
                        value={data.blocked}
                    />
                </View>
                <View style={styles.itemWrap}>
                    <Text style={styles.itemTitle}>
                        Admin rules:
                    </Text>
                    <Picker
                        placeholder="Permission"
                        onValueChange={(value) => this.setState({data: {...this.state.data, ['role']: value}})}
                        textStyle={{textAlign: 'left', fontSize: 12, alignItems: 'flex-start'}}
                        style={styles.searchBarPicker}
                    >
                        <Picker.Item label="admin" value="admin"/>
                        <Picker.Item label="user" value="user"/>
                    </Picker>
                </View>
                <View style={styles.button}>
                    <TouchableOpacity onPress={this.handleSubmit}>
                        <Text style={styles.buttonText}>Update profile</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        marginVertical: hp('1%'),
        alignSelf: 'center',
        width: wp('80%'),
        height: hp('35%'),
        backgroundColor: "#e3e3e3",
        resizeMode: 'contain'
    },
    switch: {
        alignSelf: 'flex-end',
    },
    itemWrapSwitch: {
        marginVertical: hp('1%'),
        marginHorizontal: wp('3%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemWrap: {
        marginVertical: hp('1%'),
        marginHorizontal: wp('3%'),
        flexDirection: 'row',
        alignItems: 'center'
    },
    itemTitle: {
        fontSize: wp('5%'),
        fontWeight: 'bold'
    },
    itemText: {
        fontSize: wp('5%'),
        marginLeft: wp('2%')
    },
    input: {
        width: wp('65%'),
        height: hp('4%'),
        paddingHorizontal: wp('2%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        marginLeft: wp('2%')
    },
    buttonWrap: {
        marginVertical: hp('3%'),
    },
    itemListWrap: {
        width: wp('100%'),
        height: hp('8%'),
        marginVertical: hp('0.5%'),
        paddingHorizontal: wp('3%'),
        flexDirection: 'row',
        borderColor: '#e6e6e6',
        borderWidth: 1,
        alignSelf: 'center',
        alignItems: 'center'
    },
    itemListCount: {
        justifyContent: 'flex-end',
        marginLeft: 'auto'
    },
    button: {
        width: wp('50%'),
        height: hp('7%'),
        borderWidth: 1,
        borderRadius: wp('3%'),
        borderColor: 'red',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: hp('2%')
    },
    buttonText: {
        fontSize: wp('4%'),
        fontWeight: 'bold',
        alignSelf: 'center',
        color: '#5a5355'
    },
    photo: {
        backgroundColor: 'white'
    }
});

export const AdminOneUser = (connect((state) => ({
}))(AdminOneUserComponent));