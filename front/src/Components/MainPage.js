import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text, FlatList, TextInput, StyleSheet, TouchableWithoutFeedback, Image, Keyboard, Platform} from "react-native";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Picker} from 'native-base';
import {ListItem} from 'react-native-elements'
import {getAllItem} from "../store/actions/item";
import {sortItems} from "../utils/sorfItems";
import config from '../utils/config'


class MainPageComponent extends Component {
    state = {
        searchData: {
            name: '',
            priceAt: '',
            priceTo: '',
            type: ''
        },
        sortItemArray: [],
        refreshing: false
    };

    componentDidMount() {
        this.subs = [
            this.props.navigation.addListener('willFocus', this.componentWillFocus)
        ]
    }

    componentWillFocus = () => {
        this.props.dispatch(getAllItem())
    };

    getSortItem = () => {
        this.setState({refreshing: true}, () => this.callBackSortItems());
        this.props.dispatch(getAllItem());
        Keyboard.dismiss()
    };

    callBackSortItems = () => {
        const {searchData} = this.state;
        const {allItems} = this.props;
        const item = sortItems(searchData, allItems);
        this.setState({sortItemArray: item, refreshing: false})
    };

    clearSearch = () => {
        this.setState({sortItemArray: [], searchData: {...this.state.searchData, name: '', priceAt: '', priceTo: '', type: ''}})
    };

    handleOnChange = (name, value) => {
        this.setState({searchData: {...this.state.searchData, [name]: value}});
    };

    getAllItemsRequest = () => {
        this.setState({refreshing: false})
    };

    handleRefresh = () => {
      this.props.dispatch(getAllItem());
      this.clearSearch();
      this.setState({refreshing: true,}, () => this.getAllItemsRequest())
    };

    renderItem = ({item}) => (
        <ListItem
            title={item.title}
            titleStyle={styles.itemStyle}
            rightTitle={`${item.price} UAH`}
            rightTitleStyle={styles.itemRightTitle}
            subtitle={`Type: ${item.type}`}
            subtitleStyle={styles.itemSubtitle}
            rightSubtitle={`Owner: ${item.owner}`}
            rightSubtitleStyle={styles.itemRightSubtitle}
            leftAvatar={{source: {uri: `${config.baseUrl}/api/image/?id=${item._id}`}}}
            onPress={() => this.props.navigation.navigate('OneItem', {item})}
            key={item._id}
        />
    );

    render() {
        console.log('main page render')
        const {searchData, sortItemArray} = this.state;
        return (
            <View style={styles.container}>

                <View style={styles.searchWrap}>
                    <TextInput
                        style={styles.nameSearchInput}
                        placeholder="Search..."
                        value={searchData.name}
                        onChangeText={(value) => this.handleOnChange('name', value)}
                        returnKeyType='search'
                        placeholderTextColor='black'    
                        onSubmitEditing={() => this.getSortItem()}
                    />
                    <TouchableWithoutFeedback onPress={() => this.getSortItem()}>
                        <Text style={styles.searchButton}>
                            Search
                        </Text>
                    </TouchableWithoutFeedback>
                    {
                        sortItemArray.length >= 1 ?
                            (
                            <TouchableWithoutFeedback onPress={() => this.clearSearch()}>
                                <Image
                                    style={styles.clearButton}
                                    source={require('../images/main/delete.png')}
                                />
                            </TouchableWithoutFeedback>
                            )
                            : null
                    }
                </View>
                <View style={styles.searchBarWrap}>
                    <TextInput
                        placeholder="Price at"
                        keyboardType='numeric'
                        value={searchData.priceAt}
                        style={styles.searchBarItem}
                        placeholderTextColor='black'
                        onChangeText={(value) => this.handleOnChange('priceAt', value)}
                    />
                    <TextInput
                        placeholder="Price to"
                        keyboardType='numeric'
                        name="priceTo"
                        value={searchData.priceTo}
                        style={styles.searchBarItem}
                        placeholderTextColor='black'
                        onChangeText={(value) => this.handleOnChange('priceTo', value)}
                    />
                    <Picker
                        placeholder="Type"
                        selectedValue={searchData.type}
                        onValueChange={(value) => this.handleOnChange('type', value)}
                        textStyle={{textAlign: 'left', fontSize: 12, alignItems: 'flex-start'}}
                        style={styles.searchBarPicker}
                    >
                        <Picker.Item label="All" value=""/>
                        <Picker.Item label="Phone" value="phone"/>
                        <Picker.Item label="Laptop" value="laptop"/>
                        <Picker.Item label="Monoblock" value="monoblock"/>
                        <Picker.Item label="Monitor" value="monitor"/>
                        <Picker.Item label="Accessories" value="accessories"/>
                        <Picker.Item label="Other" value="other"/>
                    </Picker>
                </View>

                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={sortItemArray.length ? sortItemArray : this.props.allItems}
                    extraData={this.state.refresh}
                    renderItem={this.renderItem}
                    refreshing={this.state.refreshing}
                    onRefresh={this.handleRefresh}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    searchWrap: {
        flexDirection: 'row',
        width: wp('100%'),
        paddingTop: hp('2%'),
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    nameSearchInput: {
        width: wp('80%'),
        height: Platform.OS === 'android' ? hp('8%') : hp('6%'),
        borderRadius: wp('3%'),
        backgroundColor: '#e6e6e6',
        paddingHorizontal: wp('2%'),
        fontSize: wp('5%')
    },
    clearButton: {
        position: 'absolute',
        width: wp('5%'),
        height: wp('5%'),
        left: wp('73%'),
        top: Platform.OS === 'android' ? hp('4.5%') : hp('3.5%'),
    },
    searchButton: {
        width: wp('15%'),
        fontSize: wp('4%'),
        color: 'blue'
    },
    searchBarWrap: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: wp('100%'),
        paddingVertical: 10,
        paddingHorizontal: 0
    },
    searchBarItem: {
        textAlign: 'center',
        width: Platform.OS === 'android' ? wp('30%') : 90,
        borderRadius: 18,
        backgroundColor: '#e6e6e6',
        height: Platform.OS === 'android' ? hp('5.5%') : 25,
        paddingHorizontal: 5,
        fontSize: 12,
    },
    searchBarPicker: {
        backgroundColor: '#e6e6e6',
        height: Platform.OS === 'android' ? hp('5.5%') : hp('3%'),
        width: Platform.OS === 'android' ? wp('20%') : wp('30%'),
        paddingVertical: 0,
        paddingHorizontal: 0,
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        borderRadius: wp('3%'),
    },
    itemStyle: {
        fontSize: wp('3.5%')
    },
    itemRightTitle: {
        fontSize: wp('3%')
    },
    itemSubtitle: {
        fontSize: wp('3%')
    },
    itemRightSubtitle: {
        fontSize: wp('3%')
    }
});

export const MainPage = (connect((state) => ({
    success: state.auth.success,
    error: state.auth.error,
    fetchInProcess: state.auth.fetchInProcess,
    allItems: state.item.allItems
}))(MainPageComponent));