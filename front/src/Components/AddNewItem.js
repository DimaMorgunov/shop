import React, {Component} from 'react';
import {View, Text, ScrollView, TouchableOpacity, StyleSheet, TextInput, Dimensions, TouchableWithoutFeedback, Platform, Keyboard} from 'react-native';
import Image from "react-native-elements/src/image/Image";
import {connect} from 'react-redux';
import {Picker} from 'native-base';
import ImagePicker from 'react-native-image-picker';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {addItem} from "../store/actions/item";
import {ValidationFields} from '../utils/ValidationFields';

const options = {
    title: 'Select image',
};

class AddNewItemComponent extends Component {

    state = {
        itemData: {
            title: '',
            price: '',
            type: '',
            characteristic: [],
            description: [],
            photo: {uri: '', photoBase64: ''},
            owner: `${this.props.user.first_name} ${this.props.user.last_name}`,
        },
        inputError: {
            title: '',
            price: '',
            type: '',
            photo: ''
        },
        characteristicTitle: '',
        characteristicText: '',
        descriptionTitle: '',
        descriptionText: '',
    };

    componentWillReceiveProps(nextProps) {
        if(this.props.user !== nextProps.user && nextProps.user) {
            this.props.navigation.navigate('Profile')
        }
    }

    handlePhoto = () => {
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            const photos = {uri: response.uri, photoBase64: response.data}
            this.setState({itemData: {...this.state.itemData, ['photo']: photos}});
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                console.log('1')
            }
        });
    };

    handleInput = (name, value) => {
        this.setState({itemData: {...this.state.itemData, [name]: value}})
    };

    handleOnChange = (name, value) => {
        this.setState({itemData: {...this.state.itemData, [name]: value}});
    };

    addNewCharacteristic = (type) => {
        let currentArray = this.state.itemData[type];
        console.log(currentArray);
        let title, text;
        switch (type) {
            case "description":
                title = this.state.descriptionTitle;
                text = this.state.descriptionText;
                break;
            case "characteristic":
                title = this.state.characteristicTitle;
                text = this.state.characteristicText;
                break;
            default:
                break;
        }
        if (title.length && text.length) {
            currentArray.push({title, text});
            this.setState({currentArray});
        }
        console.log(this.state)
    };

    handleSubmit = () => {
        const errorField = ValidationFields(this.state.itemData);
        console.log(errorField)
        this.setState({inputError: {...this.state.inputError, ...errorField.obj}}, () => console.log(this.state));
        if (errorField.arr.length == 0) {
            const {itemData } = this.state;
            itemData.email = this.props.user.email;
            this.props.dispatch(addItem(itemData))
        }
    };

    render() {
        const {itemData, inputError, characteristicTitle, characteristicText, descriptionTitle, descriptionText} = this.state;
        return (
            <ScrollView style={styles.container}>
                <View>
                    <TouchableOpacity onPress={this.handlePhoto}>
                        <Image
                            style={[styles.image,  inputError.photo ? styles.inputError : null]}
                            source={{uri: itemData.photo.uri ? itemData.photo.uri : ''}}
                        />
                    </TouchableOpacity>
                </View>
                <View>
                    <Text style={styles.title}>Product title:</Text>
                    <TextInput
                        placeholder="Title"
                        style={[styles.input, inputError.title ? styles.inputError : null]}
                        value={itemData.title}
                        onChangeText={(value) => this.handleInput('title', value)}
                    />
                    <Text style={styles.title}>Price:</Text>
                    <TextInput
                        placeholder="Price"
                        style={[styles.input, inputError.price ? styles.inputError : null]}
                        value={itemData.price}
                        onChangeText={(value) => this.handleInput('price', value)}
                        keyboardType = 'numeric'
                    />
                    <Text style={styles.title}>Product type:</Text>
                    <Picker
                        placeholder="Type"
                        selectedValue={itemData.type}
                        onValueChange={(value) => this.handleOnChange('type', value)}
                        style={[styles.picker, inputError.type ? styles.inputError : null]}
                    >
                        <Picker.Item label="Pleace choose" value=""/>
                        <Picker.Item label="Phone" value="phone"/>
                        <Picker.Item label="Laptop" value="laptop"/>
                        <Picker.Item label="Monoblock" value="monoblock"/>
                        <Picker.Item label="Monitor" value="monitor"/>
                        <Picker.Item label="Accessories" value="accessories"/>
                        <Picker.Item label="Other" value="other"/>
                    </Picker>
                </View>
                <View>
                    <Text style={styles.title}>Characteristics:</Text>
                    {
                        itemData.characteristic.length ?
                            itemData.characteristic.map(item => (
                                <View style={styles.characteristicItem}>
                                    <Image
                                        style={styles.deleteItemItem}
                                        source={require('../images/main/delete.png')}
                                    />
                                    <Text style={styles.characteristicItemTitle}>{item.title}</Text>
                                    <Text style={styles.characteristicItemText}>{item.text}</Text>
                                </View>
                            ))
                            : null
                    }
                    <Text style={styles.subTitle}>Characteristic title:</Text>
                    <TextInput
                        placeholder="Title"
                        style={styles.input}
                        value={characteristicTitle}
                        onChangeText={(value) => this.setState({characteristicTitle: value})}
                        returnKeyType='next'
                    />
                    <Text style={styles.subTitle}>Characteristic value:</Text>
                    <TextInput
                        placeholder="Text"
                        style={styles.input}
                        value={characteristicText}
                        onChangeText={(value) => this.setState({characteristicText: value})}
                        returnKeyType='next'
                    />

                    <TouchableOpacity style={styles.addButton}
                                      onPress={() => this.addNewCharacteristic('characteristic')}>
                        <Text>Add characteristic</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <Text style={styles.title}>Description:</Text>
                    {
                        itemData.description.length ?
                            itemData.description.map(item => (
                                <TouchableWithoutFeedback onPress={this.deleteCurrentItem}>
                                    <View style={styles.characteristicItem}>
                                        <Image
                                            style={styles.deleteItemItem}
                                            source={require('../images/main/delete.png')}
                                        />
                                        <Text style={styles.characteristicItemTitle}>{item.title}</Text>
                                        <Text style={styles.characteristicItemText}>{item.text}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            ))
                            : null
                    }
                    <Text style={styles.subTitle}>Description title:</Text>
                    <TextInput
                        placeholder="Title"
                        style={styles.input}
                        value={descriptionTitle}
                        onChangeText={(value) => this.setState({descriptionTitle: value})}
                        returnKeyType='next'
                    />
                    <Text style={styles.subTitle}>Description value:</Text>
                    <TextInput
                        placeholder="Text"
                        style={styles.input}
                        value={descriptionText}
                        onChangeText={(value) => this.setState({descriptionText: value})}
                        returnKeyType='next'
                    />
                    <TouchableOpacity style={styles.addButton} onPress={() => this.addNewCharacteristic('description')}>
                        <Text>Add description</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={styles.createButton} onPress={this.handleSubmit}>
                        <Text style={styles.createButtonText}>Create</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        marginVertical: hp('1%'),
        alignSelf: 'center',
        width: wp('80%'),
        height: hp('35%'),
        backgroundColor: "#e3e3e3",
        resizeMode: 'contain'
    },
    title: {
        fontSize: hp('3%'),
        marginHorizontal: wp('2%'),
        color: 'black'
    },
    input: {
        width: wp('90%'),
        height: Platform.OS === 'android' ? hp('7.5%') : hp('5%'),
        fontSize: hp('2%'),
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: '#eee',
        paddingHorizontal: wp('5%'),
        marginVertical: hp('1%'),
        color: 'black'
    },
    picker: {
        alignSelf: 'center',
        width: wp('90%'),
        height: hp('5%'),
        paddingHorizontal: wp('5%'),
        marginVertical: hp('1%'),
        borderRadius: wp('5%'),
        backgroundColor: '#e6e6e6',
        color: 'black'
    },
    subTitle: {
        fontSize: hp('2%'),
        marginHorizontal: wp('1.5%'),
        color: 'black'
    },
    addButton: {
        alignSelf: 'center',
        alignItems: 'center',
        width: wp('100%'),
        height: hp('5%'),
        fontSize: hp('2%'),
        paddingTop: hp('1%'),
        borderRadius: wp('1%'),
        backgroundColor: '#eee',
        marginVertical: hp('1%'),
    },
    characteristicItem: {
        marginHorizontal: wp('6%'),
        marginVertical: hp('1.5%'),
    },
    characteristicItemTitle: {
        fontSize: hp('2%'),
        color: 'black'
    },
    characteristicItemText: {
        fontSize: hp('1.5%'),
        color: 'black'
    },
    deleteItemItem: {
        position: 'absolute',
        width: wp('5%'),
        height: wp('5%'),
        left: wp('83%'),
        top: hp('0.5%'),
    },
    createButton: {
        alignItems: 'center',
        paddingTop: hp('1.5%'),
        width: wp('100%'),
        height: hp('8%'),
        backgroundColor: 'blue',
        marginVertical: hp('3%')
    },
    createButtonText: {
        fontSize: hp('3%'),
        color: 'white'
    },
    inputError: {
        borderColor: 'red',
        borderWidth: 2,
    },
    photoError: {
        borderWidth: 5,
        borderColor: 'red',
    },
});


export const AddNewItem = (connect((state) => ({
    success: state.auth.success,
    error: state.auth.error,
    user: state.auth.data
}))(AddNewItemComponent));