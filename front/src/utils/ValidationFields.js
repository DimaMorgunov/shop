import {Alert} from 'react-native';

export const ValidationFields = (data) => {
  const error = {obj: {}, arr: []};
  for (name in data ) {
      switch (name) {
          case 'email':
              const email = data[name].match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
              if (email || /^[0-9]+$/.test(data[name])) { error.obj.email = false;} else { error.obj.email = 'Incorrect input'; error.arr.push(name)}
              break;
          case 'first_name':
              const first_name = data[name].length >= 3;
              if (!first_name) { error.obj.first_name = 'Incorrect First Name'; error.arr.push(`${name}`) } else { error.obj.first_name = false; }
              break;
          case 'last_name':
              const last_name = data[name].length >= 3;
              if (!last_name) { error.obj.last_name = 'Incorrect Last Name'; error.arr.push(`${name}`) } else { error.obj.last_name = false; }
              break;
          case 'password':
              const password = data[name].length >= 3;
              if (!password) { error.obj.password = 'Incorrect password'; error.arr.push(`${name}`) } else { error.obj.password = false; }
              break;
          case 'message':
              const message = data[name].length >= 3;
              if (!message) { error.obj.message = 'Incorrect message'; error.arr.push(`${name}`) } else { error.obj.message = false; }
              break;
          case 'photo':
              const photo = data[name].photoBase64 != undefined;
              if (!photo) { error.obj.photo = 'Incorrect photo'; error.arr.push(`${name}`) } else { error.obj.photo = false; }
              break;
          case 'title':
              const title = data[name].length >= 3;
              if (!title) { error.obj.title = 'Incorrect message'; error.arr.push(`${name}`) } else { error.obj.title = false; }
              break;
          case 'price':
              const price = data[name].length >= 3;
              if (!price) { error.obj.price = 'Incorrect message'; error.arr.push(`${name}`) } else { error.obj.price = false; }
              break;
          case 'type':
              const type = data[name].length >= 3;
              if (!type) { error.obj.type = 'Incorrect message'; error.arr.push(`${name}`) } else { error.obj.type = false; }
              break;
          default:
              break;
      }
  }
  if (error.arr.length > 0) {
      Alert.alert(
          'Error!',
          "Incorrectly entered data",
          [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
      );
  }
  return error;
};
