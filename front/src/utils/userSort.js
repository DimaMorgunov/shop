export function sortUser(params, arrayItems) {
        if(params) {
            switch (params) {
                case 'emailAZ':
                    arrayItems.sort(function(a, b){
                        var nameA = a.email.toLowerCase(), nameB = b.email.toLowerCase()
                        if (nameA < nameB) //сортируем строки по возрастанию
                            return -1
                        if (nameA < nameB)
                            return 1
                        return 0 // Никакой сортировки
                    });
                    break;
                case 'emailZA':
                    arrayItems.sort(function(a, b){
                        var nameA=a.email.toLowerCase(), nameB=b.email.toLowerCase()
                        if (nameA > nameB) //сортируем строки по возрастанию
                            return -1
                        if (nameA < nameB)
                            return 1
                        return 0 // Никакой сортировки
                    })
                    break;
                case 'create10':
                    arrayItems.sort(function(a,b){
                        return new Date(a.createdAt) - new Date(b.createdAt);
                    });
                    break;
                case 'create01':
                    arrayItems.sort(function(a,b){
                        return new Date(b.createdAt) - new Date(a.createdAt);
                    });
                    break;
                case 'update10':
                    arrayItems.sort(function(a,b){
                        return new Date(a.updatedAt) - new Date(b.updatedAt);
                    });
                    break;
                case 'update01':
                    arrayItems.sort(function(a,b){
                        return new Date(b.updatedAt) - new Date(a.updatedAt);
                    });
                    break;
                default:
                    break;
            }
    }
    return arrayItems
}