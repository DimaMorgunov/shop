/* eslint-disable eqeqeq,guard-for-in,no-restricted-syntax */
const errorsCodes = {
  300: 'HTTP метод не поддерживается',
  301: 'Внутренняя ошибка JSON',
  302: 'Доступ запрещён',
  330: 'токен невалидный',
  331: 'Ошибка регистрации',
  332: 'Ошибка авторизации',
  333: 'Ошибка авторизации через ВК',
  350: 'Ошибка получения свободного места на файловом сервере',
  351: 'Ошибка сессии загрузки файлов',
  352: 'сессия загрузки файлов невалидна',
  353: 'Ошибка парсинга Multipart Form Data',
  354: 'Ошибка обработки файла',
  355: 'Ошибка при оповещении fs-main клиентом fs-client',
  380: 'Ошибка парсинга uuid - невалидный',
  400: 'Ошибка добавления объекта',
  401: 'Ошибка редактирования объекта',
  402: 'Ошибка удаления объекта',
  420: 'Ошибка подтверждения профиля',
  421: 'Ошибка при отправки кода подтверждения',
};

export const getErrorMessage = err => {
  if (typeof err !== 'object') return err;
  let message;
  let counter = 0;
  const error = JSON.parse(err.response.text);
  for (const errCode in errorsCodes) {
    counter++;
    if (errCode == error.error_code) {
      message = errorsCodes[errCode];
      break;
    }
    if (counter === Object.keys(errorsCodes).length) {
      message = `Код ошибки ${error.error_code}`;
    }
  }
  return message;
};
