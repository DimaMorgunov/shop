import React, {Component} from "react";
import {connect} from 'react-redux';
import {View, Text, ScrollView, StyleSheet, TouchableOpacity} from 'react-native';
import Image from "react-native-elements/src/image/Image";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {NavigationActions} from 'react-navigation';
import {logOutAction} from "../store/actions/auth";

class SideMenuComponent extends Component {

    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.closeDrawer();
    };

    handleLogout = () => {
        this.props.dispatch(logOutAction())
        this.props.navigation.navigate('Login')
    };

    render() {
        const {user} = this.props;
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View>
                        <TouchableOpacity onPress={this.navigateToScreen('Main')}>
                            <View style={styles.item}>
                                <Image
                                    style={styles.itemImage}
                                    source={require('../images/drawer/allItems.png')}
                                />
                                <Text style={styles.itemText}>
                                    All items
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('Basket')}>
                            <View style={styles.item}>
                                <Image
                                    style={styles.itemImage}
                                    source={require('../images/drawer/bin.png')}
                                />
                                <Text style={styles.itemText}>
                                    Bin
                                </Text>
                                <View style={user.bin.length ? styles.itemPushNumberWrap : null}>
                                    <Text style={styles.itemPushNumberText}>
                                        {user.bin.length ? user.bin.length : null}
                                    </Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('addItem')}>
                            <View style={styles.item}>
                                <Image
                                    style={styles.itemImage}
                                    source={require('../images/drawer/add.png')}
                                />
                                <Text style={styles.itemText}>
                                    Add item
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('Profile')}>
                            <View style={styles.item}>
                                <Image
                                    style={styles.itemImage}
                                    source={require('../images/drawer/man.png')}
                                />
                                <Text style={styles.itemText}>
                                    Profile
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.navigateToScreen('ChatList')}>
                            <View style={styles.item}>
                                <Image
                                    style={styles.itemImage}
                                    source={require('../images/drawer/group.png')}
                                />
                                <Text style={styles.itemText}>
                                    Chat
                                </Text>
                            </View>
                        </TouchableOpacity>
                        {
                            user.role === 'admin' ?
                                <TouchableOpacity onPress={this.navigateToScreen('UserList')}>
                                    <View style={styles.item}>
                                        <Image
                                            style={styles.itemImage}
                                            source={require('../images/drawer/team.png')}
                                        />
                                        <Text style={styles.itemText}>
                                            All users
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                : null
                        }
                    </View>
                </ScrollView>
                <TouchableOpacity onPress={this.handleLogout}>
                    <View style={styles.item}>
                        <Image
                            style={styles.itemImage}
                            source={require('../images/drawer/logout.png')}
                        />
                        <Text style={styles.itemText}>
                            Log out
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    itemsContainer: {},
    item: {
        flexDirection: 'row',
        width: wp('65px'),
        paddingRight: wp('3%'),
        alignItems: 'center',
        alignSelf: 'center',
        marginVertical: wp('5%')
    },
    itemPushNumberWrap: {
        alignItems: 'center',
        marginLeft: 'auto',
        fontSize: wp('6%'),
        width: wp('7.5%'),
        height: wp('7.5%'),
        backgroundColor: 'red',
        borderRadius: wp('3.75%')
    },
    itemPushNumberText: {
        alignItems: 'center',
        alignSelf: 'center',
        color: 'white',
        paddingTop: wp('1%'),
        fontSize: wp('4%'),
    },
    itemText: {
        color: 'red',
        fontSize: wp('4%'),
        marginLeft: wp('2%'),
    },
    itemImage: {
        width: wp('6%'),
        height: wp('6%')
    }
});

export const SideMenu = (connect((state) => ({
    success: state.auth.success,
    error: state.auth.error,
    user: state.auth.data
}))(SideMenuComponent));