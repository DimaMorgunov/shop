import React, {Component} from 'react';
import { Image, TouchableWithoutFeedback } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {withNavigation} from 'react-navigation'
import menu from '../images/drawer/menu.png';

class HeaderBurgerMenu extends Component {
    render(){
        return(
            <TouchableWithoutFeedback onPress={() => this.props.navigation.toggleDrawer()}>
                <Image
                    style={{
                        flex: 1,
                        alignSelf: 'center',
                        width: wp('5%'),
                        height: wp('5%'),
                        marginHorizontal: wp('3%')
                    }}
                    source={menu}
                />
            </TouchableWithoutFeedback>
        )
    }
}

export default withNavigation(HeaderBurgerMenu);