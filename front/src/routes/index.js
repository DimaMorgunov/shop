import React from 'react';
import { createStackNavigator, createAppContainer, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import { MainPage }  from '../Components/MainPage';
import { OneItem } from "../Components/OneItem/OneItem";
import { AddNewItem } from "../Components/AddNewItem";
import { Basket } from "../Components/BasketComponent";
import { Comment } from "../Components/CommentsComponent";
import {SideMenu} from "./SideMenu";
import {Login} from "../Components/Auth/LoginComponent";
import {Registration} from "../Components/Auth/RegistrationComponent";
import {Profile} from "../Components/Profile/ProfileComponent";
import HeaderBurgerMenu from "./headerBurgerMenu";
import {UserList} from "../Components/AdminPanel/UserListComponent"
import {ProfileItemList} from "../Components/Profile/ProfileItemListComponent";
import {EditUserItem} from "../Components/Profile/EditUserItemComponent"
import {Confirmation} from "../Components/Auth/ConfirmationComponent";
import {LostPassword} from "../Components/Auth/LostPasswordComponent";
import {ResetPassword} from "../Components/Profile/ResetPasswordComponent";
import {ChatList} from "../Components/Chat/ChatListComponent";
import {Chat} from "../Components/Chat/ChatComponent";
import {AdminOneUser} from "../Components/AdminPanel/AdminOneUserComponent"

const Draw = createDrawerNavigator({
        Main: { screen: MainPage },
        Basket: { screen: Basket },
        addItem: { screen: AddNewItem },
        Profile: { screen: Profile },
        UserList: { screen: UserList },
        ChatList: { screen: ChatList }
    },
    {
        contentComponent: SideMenu,
        drawerWidth: wp('75%'),
        drawerPosition: 'right',
        navigationOptions: {
                title: 'Shop',
                headerTitleStyle: {
                    fontWeight: "bold",
                    color: "#000",
                },
                headerRight: (
                    <HeaderBurgerMenu/>
                ),
            }
    });

const Navigation = createStackNavigator({
    Draw: { screen: Draw },
    OneItem: { screen: OneItem },
    Comment: { screen: Comment },
    ResetPassword: { screen: ResetPassword },
    ProfileItemList: { screen: ProfileItemList },
    EditUserItem: { screen: EditUserItem },
    Chat: { screen: Chat },
    AdminOneUser: { screen: AdminOneUser }
}, {
    initialRouteName: 'Draw',
});

const Auth = createStackNavigator({
    Login: {screen: Login},
    Registration: {screen: Registration},
    Confirmation: {screen: Confirmation},
    LostPassword: {screen: LostPassword}
});

const SwitchMenu = createSwitchNavigator({
    Auth: {screen: Auth},
    Shop: {screen: Navigation}
});

const MyApp = createSwitchNavigator({
    Nav: {screen: SwitchMenu},
});


export const Routes = createAppContainer(MyApp);
