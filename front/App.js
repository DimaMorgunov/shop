/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Platform, Alert, View} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Routes } from "./src/routes/index";
import {setSuccess} from "./src/store/actions/auth";

const Fabric = require('react-native-fabric');
const { Crashlytics } = Fabric;



class AppComponent extends Component {

    state = {
        visible: false
    };

   componentWillReceiveProps(nextProps) {
       Crashlytics.setString(JSON.stringify(nextProps));
       if (nextProps.error.hasOwnProperty('errors') && this.props.error !== nextProps.error) {
           Alert.alert(
               'Error!',
               nextProps.error.hasOwnProperty('errors') ? nextProps.error.errors.message : 'something went wrong try again',
               [
                   {text: 'OK', onPress: () => console.log('OK Pressed')},
               ],
               {cancelable: false},
           );
       }
       if (nextProps.success.hasOwnProperty('success') && this.props.success !== nextProps.success) {
           Alert.alert(
               'Success!',
               nextProps.success.hasOwnProperty('success') ? nextProps.success.success.message : 'something went wrong try again',
               [
                   {text: 'OK', onPress: () => console.log('OK Pressed')},
               ],
               {cancelable: false},
           );
       }
       if (this.props.fetchInProcess !== nextProps.fetchInProcess) {
           console.log(nextProps.fetchInProcess)
           this.setState({visible: nextProps.fetchInProcess})
       }
   }

  render() {
      console.log('app component render')
    return (
        <View style={{flex:1}}>
            <Spinner
                visible={this.state.visible}
                textContent={'Loading...'}
                textStyle={{color: '#FFF'}}
            />
           <Routes/>
        </View>
    );
  }
}

export const App = (connect((state) => ({
    success: state.auth.success,
    fetchInProcess: state.auth.fetchInProcess,
    error: state.auth.error,
    user: state.auth.data,
    allItems: state.item.allItems
}))(AppComponent));
