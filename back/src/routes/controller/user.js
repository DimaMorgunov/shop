import express from "express";
import jwt from "jsonwebtoken";
const userService = require('../service/user');
import authenticate from '../../middlewares/authenticate';
import adminPermission from "../../middlewares/adminPermission";
import logger from "../../index";
const router = express.Router();

router.post("/registration", (req, res) => {
    userService.registerUser(req.body).then(user => {
        logger.info('REGISTRATION user/api/registration', JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('REGISTRATION user/api/registration', req.body, err);
        } else {
            logger.error('REGISTRATION user/api/registration', req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.post("/login", (req, res) => {
    userService.loginUser(req.body).then(user => {
        logger.info('LOGIN user/api/login', JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('LOGIN user/api/login', req.body, err);
        } else {
            logger.error('REGISTRATION user/api/login', req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.put("/lost_password", (req, res) => {
    userService.lostPassword(req.body, res).then(user => {
        logger.info('LOST PASSWORD user/api/lost_password', req.body, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('LOST PASSWORD user/api/lost_password', req.body, err);
        } else {
            logger.error('LOST PASSWORD user/api/lost_password', req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.post("/auth_social", (req, res) => {
   userService.authSocial(req.body).then(user => {
       logger.info('AUTH SOCIAL user/api/auth_social', req.body, JSON.stringify(user));
       return res.json(user)
   }).catch(err => {
       if (err.hasOwnProperty('errors')) {
           logger.warn('AUTH SOCIAL user/api/auth_social', req.body, err);
       } else {
           logger.error('AUTH SOCIAL user/api/auth_social', req.body, err);
       }
       return res.status(500).json(err)
   })
});

router.use(authenticate);

router.put("/update_profile", (req, res) => {
    userService.updateProfile(req.headers.authorization, req.body).then(user => {
        logger.info('UPDATE PROFILE user/api/update_profile', req.headers.authorization, req.body, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('UPDATE PROFILE user/api/update_profile', req.headers.authorization, req.body, err);
        } else {
            logger.error('UPDATE PROFILE user/api/update_profile', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.post("/check_code", (req, res) => {
    userService.checkCode(req.headers.authorization, req.body).then(user => {
        logger.info('CHECK CODE user/api/check_code', req.headers.authorization, req.body, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('CHECK CODE user/api/check_code', req.headers.authorization, req.body, err);
        } else {
            logger.error('CHECK CODE user/api/check_code', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.post('/get_code', (req, res) => {
    userService.getCode(req.headers.authorization).then(user => {
        logger.info('GET CODE user/api/get_code', req.headers.authorization, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('GET CODE user/api/get_code', req.headers.authorization, err);
        } else {
            logger.error('GET CODE user/api/get_code', req.headers.authorization, err);
        }
        return res.status(500).json(err)
    })
});

router.put('/reset_password', (req, res) => {
    userService.resetPassword(req.headers.authorization, req.body).then(user => {
        logger.info('RESET PASSWORD user/api/reset_password', req.headers.authorization, req.body, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('RESET PASSWORD user/api/reset_password', req.headers.authorization, req.body, err);
        } else {
            logger.error('RESET PASSWORD user/api/reset_password', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.post("/check_token", (req, res) => {
    userService.checkToken(req.headers.authorization).then(user => {
        logger.info('CHECK TOKEN user/api/check_token', req.headers.authorization, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('CHECK TOKEN user/api/check_token', req.headers.authorization, err);
        } else {
            logger.error('CHECK TOKEN user/api/check_token', req.headers.authorization, err);
        }
        return res.status(500).json(err)
    })
});

router.put("/add_bin", (req, res) => {
    userService.addToBin(req.headers.authorization, req.body.itemId).then(user => {
        logger.info('ADD TO BIN api/user/add_bin', req.headers.authorization, req.body.itemId, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('ADD TO BIN user/api/add_bin', req.headers.authorization, req.body, err);
        } else {
            logger.error('ADD TO BIN user/api/add_bin', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.put('/remove_from_bin', (req, res) => {
    userService.removeFromBin(req.headers.authorization.split(' ')[1], req.body).then(user => {
        logger.info('REMOVE FROM BIN api/user/remove_from_bin', req.headers.authorization, req.body.itemId, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('REMOVE FROM BIN api/user/remove_from_bin', req.headers.authorization, req.body, err);
        } else {
            logger.error('REMOVE FROM BIN api/user/remove_from_bin', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.use(adminPermission);

router.put("/edit_user", (req, res) => {
    userService.updateProfile(req.headers.authorization, req.body).then(user => {
        logger.info('EDIT USER user/api/edit_user', req.headers.authorization, req.body, JSON.stringify(user));
        return res.json(user)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('EDIT USER user/api/edit_user', req.headers.authorization, req.body, err);
        } else {
            logger.error('EDIT USER user/api/edit_user', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.post("/user_list", (req, res) => {
   userService.getUsers().then(users => {
       logger.info('USER LIST user/api/user_list', req.headers.authorization, req.body, JSON.stringify(users));
       return res.json(users)
   }).catch(err => {
       if (err.hasOwnProperty('errors')) {
           logger.warn('USER LIST user/api/user_list', req.headers.authorization, req.body, err);
       } else {
           logger.error('USER LIST user/api/user_list', req.headers.authorization, req.body, err);
       }
       return res.status(500).json(err)
   })
});



export default router;
