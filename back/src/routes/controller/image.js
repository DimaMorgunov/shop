import express from "express";

const router = express.Router();
const ImageService = require('../service/image')

router.get("/", (req, res) => {
    ImageService.getImage(req.query.id).then(item => res.contentType('image/jpeg').send(item[0].imageBinary)).catch(() => res.json('error'))
});

export default router;