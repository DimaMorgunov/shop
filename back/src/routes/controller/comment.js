import express from "express";
import logger from "../../index";

const router = express.Router();
const commentService = require('../service/comment');

router.get("/", (req, res) => {
    commentService.getComment(req.query.id).then(comment => {
        logger.info('GET COMMENTS comment/api/?req.query.id', req.query.id, JSON.stringify(comment));
        return res.json(comment)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('GET COMMENTS comment/api/?req.query.id',req.query.id, err);
        } else {
            logger.error('GET COMMENTS comment/api/?req.query.id',req.query.id, err);
        }
        return res.status(500).json(err)
    })
});

router.post("/add_comment", (req, res) => {
    commentService.addComment(req.body).then(comment => {
        logger.info('ADD COMMENT comment/api/add_comment', req.headers.authorization, req.body, JSON.stringify(comment));
        return res.json(comment)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('ADD COMMENT comment/api/add_comment', req.headers.authorization, req.body, err);
        } else {
            logger.error('ADD COMMENT comment/api/add_comment', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.post("/add_subcomment", (req, res) => {
    commentService.addSubComment(req.body).then(comment => {
        logger.info('ADD SUBCOMMENT comment/api/add_subcomment', req.headers.authorization, req.body, JSON.stringify(comment));
        return res.json(comment)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('ADD SUBCOMMENT comment/api/add_subcomment', req.headers.authorization, req.body, err);
        } else {
            logger.error('ADD SUBCOMMENT comment/api/add_subcomment', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.put("/update_comment", (req, res) => {
    commentService.updateComment(req.body).then(comment => {
        logger.info('UPDATE COMMENT comment/api/add_subcomment', req.headers.authorization, req.body, JSON.stringify(comment));
        return res.json(comment)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('UPDATE COMMENT comment/api/add_subcomment', req.headers.authorization, req.body, err);
        } else {
            logger.error('UPDATE COMMENT comment/api/add_subcomment', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

router.delete("/delete_comment", (req, res) => {
    commentService.deleteComment(req.body, res).then(comment => {
        logger.info('DELETE COMMENT comment/api/delete_comment', req.headers.authorization, req.body, JSON.stringify(comment));
        return res.json(comment)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('DELETE COMMENT comment/api/delete_comment', req.headers.authorization, req.body, err);
        } else {
            logger.error('DELETE COMMENT comment/api/delete_comment', req.headers.authorization, req.body, err);
        }
        return res.status(500).json(err)
    })
});

export default router;