import express from "express";

const router = express.Router();
const ItemService = require('../service/item');
import logger from "../../index";
import authenticate from "../../middlewares/authenticate";


router.get("/all", (req, res) => {
    ItemService.getItemAll().then(item => {
        logger.info('GET ALL ITEMS item/api/all', JSON.stringify(item));
        return res.json(item)
    }).catch(err => {
        logger.error('GET ALL ITEMS /all ', err);
        res.json({errors: {message: 'Cant get items'}})
    })
});

router.get("/", (req, res) => {
    ItemService.getCurrentItem(req.query.id).then(item => {
        logger.info('GET CURRENT ITEM item/api/?req.query.id', req.query.id ,JSON.stringify(item));
        return res.json(item)
    }).catch(err => {
        logger.error('GET CURRENT ITEMS item/api/?req.query.id', req.query.id, err);
        return res.json({errors: {message: 'Cant get item'}})
    })
});

router.use(authenticate);

router.post("/user_items", (req, res) => {
    ItemService.getUserItems(req.body, res).then(item => {
        logger.info('GET USER ITEMS item/api/user_items', JSON.stringify(req.body), JSON.stringify(req.headers.authorization) ,JSON.stringify(item));
        return res.json(item)
    }).catch(err => {
        logger.error('GET USER ITEMS item/api/user_items', err);
        return res.json(err)
    })
});

router.post("/add_star", (req, res) => {
    ItemService.addStar(req.headers.authorization.split(' ')[1], req.body).then(item => {
        logger.info('ADD STAR TO ITEM item/api/add_star',JSON.stringify(req.body), JSON.stringify(req.headers.authorization) ,JSON.stringify(item));
        return res.json(item)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('ADD STAR TO ITEM item/api/add_star', err);
        } else {
            logger.error('ADD STAR TO ITEM item/api/add_star', err);
        }
        return res.status(500).json(err)
    })
});

router.post("/add", (req, res) => {
    ItemService.addItem(req.body, req.headers.authorization.split(' ')[1]).then(item => {
        logger.info('ADD ITEM item/api/add',JSON.stringify(req.body), JSON.stringify(req.headers.authorization) ,JSON.stringify(item));
        return res.json(item)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('ADD ITEM item/api/add', err);
        } else {
            logger.error('ADD ITEM item/api/add', err);
        }
        return res.status(500).json(err)
    })
});

router.put("/update", (req, res) => {
    ItemService.updateItem(req.headers.authorization.split(' ')[1], req.body.data).then(item => {
        logger.info('UPDATE ITEM item/api/update',JSON.stringify(req.body),JSON.stringify(req.headers.authorization) ,JSON.stringify(item));
        return res.json(item)
    }).catch(err => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('UPDATE ITEM item/api/update',JSON.stringify(req.body), JSON.stringify(req.headers.authorization) , err);
        } else {
            logger.error('UPDATE ITEM item/api/update',JSON.stringify(req.body), JSON.stringify(req.headers.authorization) , err);
        }
        console.log('2312')
        return res.status(500).json(err)
    })
});

router.delete("/delete", (req, res) => {
    ItemService.deleteItem(req.body.id).then((item) => {
        logger.info('DELETE ITEM item/api/delete',JSON.stringify(req.body), JSON.stringify(req.headers.authorization) ,JSON.stringify(item));
        return res.json(item)
    }).catch((err) => {
        if (err.hasOwnProperty('errors')) {
            logger.warn('DELETE ITEM item/api/delete', err);
        } else {
            logger.error('DELETE ITEM item/api/delete', err);
        }
        return res.json(err)
    })
});

export default router;