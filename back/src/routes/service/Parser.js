import Item from "../../models/Item";
import Image from '../../models/Image';
import Comment from "../../models/Comment";
import axios from 'axios';
import {parse} from 'node-html-parser';

const image2base64 = require('image-to-base64');


export async function parseUrlItems() {
    for (let i = 1; i <= 10; i++) {
        const request = await axios.get(`https://stylus.ua/noutbuki/p${i}/`)
        pageParse(request.data)
    }
}

const pageParse = async (html) => {
    const elements = parse(html);
    for (let i = 1; i <= elements.querySelectorAll('.name-block').length; i++) {
        const attributs = elements.querySelectorAll('.name-block')[1].childNodes[0].rawAttributes;
        const price = elements.querySelectorAll('.regular-price')[1].childNodes[0].rawText.replace(" ", '');
        const href = attributs.href;
        const title = attributs.title;
        const pageCharacteristic = await parseItem(href);
        pageCharacteristic.price = price;
        pageCharacteristic.url = href;
        pageCharacteristic.title = title;
        pageCharacteristic.owner = 'store';
        pageCharacteristic.type = 'laptop';
        const user = new Item(pageCharacteristic);
        user.save().then(item => {
            pageCharacteristic.imageId = item._id;
            const image = new Image(pageCharacteristic)
            image.save().then(image => {
                const comment = new Comment({idItem: item._id});
                comment.save().then().catch(console.log(err))
            }).catch(err => console.log(err))
        }).catch(err => console.log(err))
    }
};

async function parseItem(urlItem) {
    const request = await axios.get(`https://stylus.ua${urlItem}`)
    const elements = parse(request.data);
    const itemsObj = {
        characteristic: [],
        description: [],
        imageBinary: '',
        imagesUrl: '',
        imageId: ''
    };
    // characteristics array tbody: name | value
    const allCharacteristic = elements.querySelector('.characteristics-block').childNodes[1].childNodes[0].childNodes;
    if (allCharacteristic.length !== 0) {
        for (let i = 0; i < allCharacteristic.length; i++) {
            if (allCharacteristic[i].childNodes[0].childNodes[0].rawText !== undefined && allCharacteristic[i].childNodes[1].childNodes[0].rawText !== undefined) {
                itemsObj.characteristic.push({
                    [allCharacteristic[i].childNodes[0].childNodes[0].rawText]: allCharacteristic[i].childNodes[1].childNodes[0].rawText
                })
            }
        }
    }
    // description
    const description = elements.querySelector('.text-block').childNodes;
    if (description.length !== 0) {
        for (let i = 0; i < description.length; i += 2) {
            if (description[i].rawText !== undefined && description[i + 1].rawText !== undefined) {
                itemsObj.description.push({
                    ['title']: description[i].rawText, ['text']: description[i + 1].rawText
                })
            }
        }
    }
    // image
    const imagesUrl = elements.querySelector('source').rawAttributes['data-srcset'].split(',')[0];
    var binary;
    const image = await image2base64(`https://stylus.ua${imagesUrl}`)
    binary = new Buffer(image, 'base64');
    itemsObj.imageBinary = binary;
    itemsObj.imagesUrl = imagesUrl;
    return itemsObj
}
