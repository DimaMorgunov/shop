const tokenDecoder = require("../../utils/decodeToken");

const RepositoryItem = require("../repository/item");
const RepositoryImage = require("../repository/image");
const RepositoryUser = require("../repository/user");
const RepositoryComment = require("../repository/comment");


exports.getItemAll = async () => {
    return await RepositoryItem.findAll()
};

exports.getCurrentItem = (id) => {
    return RepositoryItem.findCurrentItem(id)
};

exports.addItem = async (fields) => {
    const image = {};
    if (fields.title.length < 3 && fields.price.length < 1 && fields.type.length < 1) {
        throw {errors: {message: 'Incorrect data'}}
    }
    if (fields.photo.photoBase64 === undefined) {
        throw {errors: {message: 'Cant add item without image'}}
    } else {
        image.imageBinary = new Buffer(fields.photo.photoBase64, "base64");
        return await RepositoryItem.addItem(fields)
            .then(item => {
                image.imageId = item._id;
                return RepositoryImage.addImage(image)
                    .then((image) => {
                        return RepositoryUser.findAndUpdate(fields.email, item._id)
                            .then(user => {
                                if (user === null) {
                                    return RepositoryItem.removeItem(item.id).then(item => {
                                        throw {errors: {message: 'cant add new file'}}
                                    })
                                } else {
                                    return RepositoryComment.createComment(item._id).then(comment =>
                                        ({user: user.toAuthJSON()}))
                                        .catch(err => {
                                            return RepositoryItem.removeItem(item.id).then(item => {
                                                return RepositoryImage.removeImage(item._id).then(image => {
                                                    {
                                                        throw {errors: {message: 'cant add new file'}}
                                                    }
                                                })
                                            })

                                        })
                                }
                            })
                            .catch(err => {
                                return RepositoryImage.removeImage(item._id).then(image => {
                                    return RepositoryItem.removeItem(item._id).then(item => {
                                        throw {errors: {message: 'cant add new file'}}
                                    })
                                });
                            })
                    })
                    .catch((err => {
                        return RepositoryItem.removeItem(item.id).then(item => {
                            throw {errors: {message: 'cant add new file'}}
                        })
                    }));
            })
            .catch(err => err);
    }

};

exports.getUserItems = async (fields) => {
    return await RepositoryItem.getUserItems(fields).then(item => item).catch(err => {
        throw {errors: {message: 'cant find items'}}
    })
};

exports.updateItem = async (token, fields) => {
    if (token) {
        const email = await tokenDecoder.getDecodetToken(token);
        const user = await RepositoryUser.findUser(email).then(user => user).catch(err => {
            throw {errors: {message: 'You dont have permission'}}
        });
        if (email.hasOwnProperty('errors')) {
            throw {errors: {message: 'Invalid credentials'}}
        } else {
            if (user.itemList.indexOf(fields._id) > 0 || user.role === 'admin') {
                if (fields.photo.photoBase64.length > 0) {
                    return RepositoryItem.findCurrentItem(fields._id).then((item) => {
                        let changetItem = item[0];
                        for (let key in fields) {
                            for (let keyItem in changetItem) {
                                if (key === keyItem) {
                                    changetItem[keyItem] = fields[key]
                                }
                            }
                        }
                        const imageBinary = new Buffer(fields.photo.photoBase64, "base64");
                        return RepositoryImage.updateImage(changetItem._id, imageBinary).then(image => {
                            return RepositoryItem.saveItem(changetItem).then(item => {
                                return {success: {message: 'Item has been update'}}
                            }).catch(err => {
                                throw {errors: {message: 'Cant add image'}}
                            })
                        }).catch(err => {
                            if (err.hasOwnProperty('errors')) {
                                return err
                            } else {
                                throw {errors: {message: 'cant find items'}}
                            }
                        });
                    }).catch(err => {
                        if (err.hasOwnProperty('errors')) {
                            return err
                        } else {
                            throw {errors: {message: 'cant add image'}}
                        }
                    });
                } else {
                    return await RepositoryItem.findCurrentItem(fields._id).then((item) => {
                        item = item[0];
                        for (let key in fields) {
                            for (let keyItem in item) {
                                if (key === keyItem) {
                                    item[keyItem] = fields[key]
                                }
                            }
                        }
                        return RepositoryItem.saveItem(item).then(item => {
                            return {success: {message: 'Item has been update'}}
                        })
                    }).catch(err => {
                        throw {errors: {message: 'cant update item'}}
                    });
                }
            } else {
                throw {errors: {message: 'Invalid credentials'}}
            }
        }
    } else {
        throw {errors: {message: 'Invalid credentials'}}
    }

};

exports.deleteItem = async (id) => {
    return await RepositoryImage.findImage(id).then(localImage => {
        return RepositoryItem.findCurrentItem(id).then(localItem => {
            return RepositoryImage.removeImage(id).then(image => {
                return RepositoryItem.removeItem(id).then(item => {
                    return RepositoryUser.removeAllDependency(id).then(user => {
                        console.log('3123', user)
                        return {success: {message: 'Item removed'}}
                    }).catch(() => {
                        return RepositoryImage.addImage(...localImage).then(image => {
                            return RepositoryItem.addItem(...localItem).then(item => {
                                throw {errors: {message: 'Cant remove image, try again'}}
                            })
                        })
                    })
                }).catch(() => {
                    RepositoryImage.addImage({localImage});
                    {
                        throw {errors: {message: 'Cant remove image, try again'}}
                    }
                })
            }).catch(err => {
                throw {errors: {message: 'Cant remove image, try again'}}
            })
        }).catch(err => {
            throw {errors: {message: 'Cant remove image, try again'}}
        })
    }).catch(() => {

            throw {errors: {message: 'Cant remove image, try again'}}

    });
};

exports.addStar = async (header, data) => {
    if (header) {
        const email = await tokenDecoder.getDecodetToken(header);
        return await RepositoryUser.findUser(email).then(user => {
            if (user === null) {
                throw {errors: {message: 'Cant find user'}}
            } else {
                return RepositoryItem.findCurrentItem(data.id).then(item => {
                    if (item === null) {
                        throw {errors: {message: 'Cant find item'}}
                    } else {
                        const {stars} = item[0];
                        if (stars.length > 0) {
                            const starArrayWithOutOtherItems = stars.filter(item => item.owner === user._id);
                            starArrayWithOutOtherItems.push({owner: user._id, rating: data.rating});
                            item[0].stars = starArrayWithOutOtherItems;
                        } else {
                            stars.push({owner: user._id, rating: data.rating});
                        }
                        return RepositoryItem.updateStars(item[0]._id, item[0].stars).then(newItem => newItem)
                            .catch(err => {
                                throw {errors: {message: 'Upssss... try again'}}
                            })
                    }
                }).catch(err => {
                    throw {errors: {message: 'Cant find item'}}
                })
            }
        }).catch(err => {
            throw {errors: {message: 'Invalid token'}}
        })
    } else {
        {
            throw {errors: {message: 'Invalid token'}}
        }
    }
};

