const RepositoryImage = require("../repository/image");
const RepositoryUser = require('../repository/user');
var User = require('../../models/User');
const parseErrors = require("../../utils/parseError");
var moment = require('moment');
const bcrypt = require("bcrypt");
const image2base64 = require('image-to-base64');
var randomstring = require("randomstring");
const {resetPasswordEmail} = require('../../utils/emailTamplate');
const {confirmationEmail} = require('../../utils/emailTamplate');
const tokenDecoder = require('../../utils/decodeToken');


exports.registerUser = async (fields) => {
    const {email, password, first_name, last_name, photo} = fields;
    await RepositoryUser.findUser(email).then(item => {
        if (item !== null) {
            throw {errors: {message: 'This email already taken'}}
        }
    });
    const data = new User({first_name, last_name, email});
    data.setPassword(password);
    data.setConfirmCode();
    const image = {};
    return await RepositoryUser.createUser(data).then(item => {
            if (photo.photoBase64) {
                image.imageBinary = new Buffer(photo.photoBase64, "base64");
                image.imageId = item._id;
                return RepositoryImage.addImage(image).then(image => {
                    return {user: item.toAuthJSON()}
                })
                    .catch(err => {
                        throw {errors: {message: 'cant add Image'}}
                    })
            } else {
                image.imageBinary = new Buffer(photo.photoBase64, "base64");
                image.imageId = item._id;
                return RepositoryImage.addImage(image).then(image => {
                    return {user: item.toAuthJSON()}
                }).catch(err => {
                    throw{errors: parseErrors(err.errors)}
                })
            }
        }
    ).catch(err => {
        if (err.errors.message === undefined) {
            return err
        } else {
            throw {errors: {message: 'Invalid data'}}
        }
    });

};

exports.loginUser = async (fields) => {
    return await RepositoryUser.findUser(fields.email).then(user => {
        if (user === null) {
            throw {errors: {message: 'Cant find user'}}
        } else {
            if (user.blocked) {
                throw {errors: {message: "Sorry, you has been blocked!"}}
            } else {
                if (user && user.isValidPassword(fields.password)) {
                    return {user: user.toAuthJSON()};
                } else {
                    throw {errors: {message: "Invalid credentials"}};
                }
            }
        }
    })
};

exports.authSocial = async (data) => {
    const fbAvatar = await image2base64(`http://graph.facebook.com/${data.id}/picture`);
    return await RepositoryUser.findUser(data.email).then(user => {
        if (user === null) {
            const randomPass = randomstring.generate({length: 8});
            data.first_name = data.name.split(" ")[0];
            data.last_name = data.name.split(" ")[1];
            const {email, first_name, last_name} = data;
            const userObject = new User({email, first_name, last_name});
            userObject.setPassword(randomPass);
            userObject.confirmation = true;
            const image = {};
            return RepositoryUser.createUser(userObject).then(item => {
                image.imageId = item._id;
                image.imageBinary = new Buffer(fbAvatar, 'base64');
                 return RepositoryImage.addImage(image).then(image => {return {user: item.toAuthJSON()}})
                     .catch(err => {throw {errors: {message: 'Cant add image'}}})
            }).catch(err => {
                throw {errors: {message: 'Cant add user'}}
            })
        } else {
            return {user: user.toAuthJSON()}
        }
    }).catch(error => {
        {throw {errors: {message: 'Cant find user'}}}
    })
};

exports.updateProfile = async (header, data) => {
    const token = header.split(' ')[1];
    if (token && data.first_name.length > 3 && data.last_name.length > 3) {
        const email = await tokenDecoder.getDecodetToken(token);
        if (email.errors === undefined) {
            return await RepositoryUser.findUser(email).then(user => {
                    if (user !== null) {
                        if (data.photo.photoBase64) {
                            return RepositoryImage.findImage(user._id).then(image => {
                                const imageBinary = new Buffer(data.photo.photoBase64, 'base64');
                                return RepositoryImage.updateImage(user._id, imageBinary).then(newImage => {
                                    for (let key in data) {
                                        for (let keyItem in user) {
                                            if (key === keyItem) {
                                                user[keyItem] = data[key]
                                            }
                                        }
                                    }
                                    return RepositoryUser.saveUser(user).then(newUser => {
                                        return {user: newUser.toAuthJSON()}
                                    }).catch(err => {
                                        return RepositoryImage.updateImage(user._id, image.imageBinary).then(image => {
                                            throw {errors: {message: 'cant update image'}}
                                        })
                                    }).catch(err => {
                                        throw {errors: {message: 'Try again'}}
                                    })
                                })
                            }).catch(err => {
                                throw {errors: {message: 'Cant find avatar, try again'}}
                            })
                        } else {
                            for (let key in data) {
                                for (let keyItem in user) {
                                    if (key === keyItem) {
                                        user[keyItem] = data[key]
                                    }
                                }
                            }
                            return RepositoryUser.saveUser(user).then(newUser => {
                                return {user: newUser.toAuthJSON()}
                            }).catch(err => {
                                return RepositoryUser.saveUser()
                            })
                        }

                    } else {
                        throw {errors: {message: 'Invalid credentials'}}
                    }
                }
            ).catch(err => {
                throw {errors: {message: 'Upppsss... try again'}}
            });
        } else {
            throw {errors: {message: 'Invalid credentials'}}
        }
    } else {
        throw {errors: {message: 'Invalid credentials'}}
    }
};

exports.checkCode = async (header, body) => {
    const token = header.split(' ')[1];
    if (token) {
        const email = await tokenDecoder.getDecodetToken(token);
        if (email.errors === undefined) {
            return await RepositoryUser.findUser(email).then(user => {
                if (user.confirmation === true) {
                    return {user: user.toAuthJSON()}
                }
                    const dateNow = moment();
                    const resultDate = dateNow.diff(user.lastConfirmationCodeUpdate, 'minutes');
                    if (resultDate > 60) {
                        throw {errors: {message: 'Invalid date'}}
                    } else {
                        if (body.num === user.confirmationCode) {
                            return RepositoryUser.confirmCode(email).then(user => {
                                return {user: user.toAuthJSON()}
                            }).catch(err => {
                                throw {errors: {message: 'Upppsss... try again'}}
                            })
                        } else {
                            return {errors: {message: 'Invalid code'}}
                        }
                    }
                }
            ).catch(err => {
                throw {errors: {message: 'Upppsss... try again'}}
            });
        } else {
            throw {errors: {message: 'Invalid credentials'}}
        }
    } else {
        return {errors: {message: 'Invalid credentials'}}
    }
};

exports.getCode = async (header) => {
    const token = header.split(' ')[1];
    if (token) {
        const email = await tokenDecoder.getDecodetToken(token);
        if (email.errors === undefined) {
            const code = `${Math.floor((Math.random() * 1000000) + 54)}`.substring(0, 5);
            confirmationEmail(email, code);
            return await RepositoryUser.sendNewCode(email, code).then(user => {
                    if (user !== null) {
                        return {success: {message: 'We send on you emil new password'}}
                    } else {
                        throw {errors: {message: 'Cant find user'}}
                    }
                }
            ).catch(err => {
                throw {errors: {message: 'Invalid credentials'}}
            })
        } else {
            throw {errors: {message: 'Invalid credentials'}}
        }
    } else {
        {
            throw {errors: {message: 'Invalid credentials'}}
        }
    }
};

exports.resetPassword = async (header, data) => {
    const token = header.split(' ')[1];
    if (token) {
        const email = await tokenDecoder.getDecodetToken(token);
        if (email.errors !== undefined) {
            throw {errors: {message: 'Invalid credentials'}}
        } else {
            if (data.oldPassword !== data.newPassword) {
                if (data.newPassword === data.repeatNewPassword) {
                    return await RepositoryUser.findUser(email).then(user => {
                        const passwordAccess = bcrypt.compareSync(data.oldPassword, user.passwordHash);
                        if (passwordAccess) {
                            const newPassword = bcrypt.hashSync(data.newPassword, 10)
                            return RepositoryUser.updatePassword(email, newPassword)
                                .then(newUser => {
                                    return {success: {message: 'New password has been changing'}}
                                })
                                .catch(err => {
                                    throw {errors: {message: 'Data base not working'}}
                                })
                        } else {
                            throw {errors: {message: 'Old password invalid'}}
                        }
                    }).catch(err => {
                        throw {errors: {message: 'Data base not working'}}
                    })
                } else {
                    throw {errors: {message: 'Data base not working'}}
                }
            } else {
                throw {errors: {message: 'Old password matches new'}}
            }
        }
    }
};

exports.lostPassword = async (data) => {
    const randomPass = randomstring.generate({length: 8});
    return await RepositoryUser.findUser(data.email).then(user => {
            const hashPassword = bcrypt.hashSync(randomPass, 10);
            return RepositoryUser.updatePassword(data.email, hashPassword).then(user => {
                    if (user !== null) {
                        resetPasswordEmail(data.email, randomPass);
                        return {success: {message: 'We send new password on email'}}
                    } else {
                        throw {errors: {message: 'Invalid credentials'}}
                    }

                }
            )
        }
    ).catch(err => {
        throw {errors: {message: 'Invalid credentials'}}
    })
};

exports.checkToken = async (fields) => {
    const token = fields.split(' ')[1];
    if (token) {
        const email = await tokenDecoder.getDecodetToken(token);
        if (email.errors !== undefined) {
            throw {errors: {message: 'Invalid credentials'}};
        } else {
            return RepositoryUser.findUser(email).then(item => {
                return {user: item.toAuthJSON()}
            }).catch(err => {
                throw {errors: {message: 'Invalid credentials'}}
            });
        }
    }
    throw {errors: {message: 'Invalid token'}}
};

exports.addToBin = async (haeder, data) => {
    const token = haeder.split(' ')[1];
    if (token) {
        const email = await tokenDecoder.getDecodetToken(token);
        console.log(1);
        if (email.errors !== undefined) {
            throw {errors: {message: 'Invalid credentials'}}
        } else {
            return RepositoryUser.addToBin(email, data)
                .then(user => {
                    return {user: user.toAuthJSON()}
                })
                .catch(err => {
                    throw {errors: {message: 'Cant add to bin, try again'}}
                })
        }
    } else {
        throw {errors: {message: 'Invalid credentials'}}
    }
};

exports.getUsers = async () => {
    return await RepositoryUser.getUsers().then(users => {
        return users
    }).catch(err => {
        throw {errors: {message: 'Cant find users!'}}
    })
};

exports.removeFromBin = async (token, data) => {
    if (token) {
        const email = await tokenDecoder.getDecodetToken(token);
        console.log(data, 'email 123')
        return await RepositoryUser.findUser(email).then(user => {
            if (user === null) {
                throw {errors: {message: 'Cant find user'}}
            } else {
                user.bin = user.bin.filter(item => item != data.itemId)
                return RepositoryUser.saveUser(user).then(user => user.toAuthJSON()).catch(err => {throw {errors: {message: 'Cant save changes!'}}})
            }
        }).catch(err => {
            throw {errors: {message: 'Invalid token'}}
        })
    } else {
        {
            throw {errors: {message: 'Invalid token'}}
        }
    }
};