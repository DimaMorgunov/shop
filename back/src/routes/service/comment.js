const RepositoryComment = require("../repository/comment");
const RepositoryUser = require("../repository/user");
const  commentUtils = require('../../utils/Comments');
const catchError = require('../../utils/parseError');
var mongoose = require('mongoose');


exports.getComment = async (id) => {
    return await RepositoryComment.findComment(id).then(comment => {
        const response = commentUtils.getCommentObj(comment[0]);
        return response
    }).catch(err => {
        throw {errors: {message: 'Cant find user'}}
    })
};

exports.addComment = async (data) => {
    return await RepositoryUser.findUser(data.email).then(user => {
        if (user === null) {
            throw {errors: {message: 'cant find user'}}
        } else {
            return RepositoryComment.findComment(data.itemId).then(comment => {
                let addSubComment = comment[0];
                addSubComment.comment.push({
                    text: data.comment,
                    createDate: new Date(),
                    ownerId: user._id,
                    id: new mongoose.Types.ObjectId,
                    name: `${user.first_name} ${user.last_name}`,
                    wasUpdate: false,
                    parentId: null
                });
                return RepositoryComment.saveComments(addSubComment)
                    .then(newComment => {
                        const response = commentUtils.getCommentObj(newComment)
                        return response
                    })
                    .catch(err => {throw {error: {message: 'cant add subcomment'}}})
            }).catch(err => {
                return catchError(err, {errors: {message: 'cant add comment'}})
            })
        }
    }).catch(err => {
        return catchError(err, {errors: {message: 'cant find user'}})
    })
};

exports.addSubComment = async (data) => {
    return await RepositoryUser.findUser(data.email).then(user => {
        return RepositoryComment.findComment(data.itemId).then(comment => {
            let addSubComment = comment[0];
            addSubComment.comment.push({
                text: data.subComment,
                createDate: new Date(),
                ownerId: user._id,
                id: new mongoose.Types.ObjectId,
                name: `${user.first_name} ${user.last_name}`,
                parentId: data.number,
                wasUpdate: false
            });
            addSubComment.markModified("comment");
            return RepositoryComment.saveComments(addSubComment)
                .then(newComment => {
                    console.log(newComment)
                    const response = commentUtils.getCommentObj(newComment);
                    return response
                })
                .catch(err => {throw {error: {message: 'cant add subcomment'}}})
        }).catch(err => {throw {error: {message: 'cant find current comment'}}});
    }).catch(err => {throw {error: {message: 'cant find user'}}})
};

exports.updateComment = async (data) => {
    return await RepositoryComment.findComment(data.itemId).then(comment => {
        if (comment !== null) {
            console.log(data)
            var updateComment = comment[0];
            for (let i = 0; i < updateComment.comment.length; i++) {
                console.log(updateComment.comment[i].id, data.index)
                if (updateComment.comment[i].id == data.index) {
                    updateComment.comment[i].text = data.editText;
                    updateComment.comment[i].wasUpdate = true
                    console.log(updateComment, '12333')
                }
            }
        } else {
            throw {errors: {message: 'Cant find comment'}}
        }
        console.log(updateComment)
        updateComment.markModified("comment");
        return RepositoryComment.saveComments(updateComment).then(item => {
            const response = commentUtils.getCommentObj(item)
            console.log(response)
            return response
        })
            .catch(err => {
                throw {errors: {message: 'Cant update comment'}}
            })
    }).catch(err => {
        throw {errors: {message: 'Cant update comment'}}
    })
};

exports.deleteComment = async (data) => {
    return await RepositoryComment.findComment(data.itemId).then(comment => {
        console.log(comment)
            let updateComment = comment[0];
            let currentItem = updateComment.comment.filter(item => item.id == data.id);
            if (currentItem.parentId != data.id) {
                updateComment = updateComment.comment.filter(item => item.id != data.id);
                console.log(updateComment, 'update comment wwewe')
                comment[0].comment = updateComment;
                comment[0].markModified("comment");
                return RepositoryComment.saveComments(comment[0]).then(item => {
                    const response = commentUtils.getCommentObj(item)
                    console.log(response, 'if response')
                    return response
                }).catch(err => {
                    return catchError(err, {errors: {message: 'cant delete comment'}})
                })
            } else {
                updateComment = updateComment.comment.filter(item => item.id != data.id);
                updateComment = updateComment.comment.filter(item => item.parentId != data.id);
                console.log(updateComment)
                comment[0].comment = updateComment;
                comment[0].markModified("comment");
                return RepositoryComment.saveComments(comment[0]).then(item => {
                    const response = commentUtils.getCommentObj(item)
                    console.log(response)
                    return response
                }).catch(err => {
                    return catchError(err, {errors: {message: 'cant delete comment'}})
                })

            }

        }
    ).catch(err => {
        console.log(err)
        return catchError(err, {errors: {message: 'cant find item'}})
    })
};
