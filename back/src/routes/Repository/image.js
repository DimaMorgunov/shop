const mongoose = require("mongoose");
var Image = require("../../models/Image");

exports.findImage = async (id) => {
    return await Image.find({imageId: id})
};
exports.removeImage = async (id) => {
    return await Image.remove({imageId: id})
};
exports.addImage = async (data) => {
    return await Image.create({...data})
};
exports.updateImage = async (id, data) => {
  return await Image.findOneAndUpdate({imageId: id}, {imageBinary: data})
};