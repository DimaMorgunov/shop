const mongoose = require("mongoose");
var Comment = require('../../models/Comment');

exports.createComment = async (data) => {
    return await Comment.create({idItem: data})
};

exports.findComment = async (id) => {
    return await Comment.find({idItem: id})
};

exports.addSubComment = async (data) => {
    return await Comment.findOneAndUpdate({idItem: data.itemId}, {$push: {comment: data.data}}, {new: true})
};

exports.saveComments = async (comment) => {
  return await comment.save()
};

exports.removeComment = async () => {
    return await Comment.remove()
};