
const mongoose = require("mongoose");
var Item = require('../../models/Item');

exports.findAll = async () => {
        return await Item.find({});
};

exports.findCurrentItem = async (id) => {
    return await Item.find({ _id: id })
};

exports.getUserItems = async (id) => {
    return await Item.find({ _id: { "$in": id } })
};

exports.addItem = async (data) => {
    return await Item.create(data)
};

exports.removeItem = async (id) => {
    return await Item.remove({_id: id})
};

exports.updateStars = async (id, stars) => {
    return await Item.findOneAndUpdate({_id: id}, {$set: {stars: stars}}, {new: true})
};

exports.saveItem = async (item) => {
    return await item.save()
};