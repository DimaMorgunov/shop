var User = require('../../models/User');
var moment = require('moment');

exports.createUser = async(data) => {
    return await User.create(data);
};

exports.findUser = async (data) => {
    return await User.findOne({email: data})
};

exports.findAndUpdate = async (data, Id) => {
    return await User.findOneAndUpdate({email: data}, {$push: {itemList: Id}}, {new: true})
};

exports.addToBin = async (email, id) => {
    console.log(email, id, 23123)
    return await User.findOneAndUpdate({email: email}, {$push: {bin: id}}, {new: true})
}

exports.confirmCode = async (data) => {
    return await User.findOneAndUpdate({email: data}, {confirmation: true}, {new: true})
};

exports.removeAllDependency = async (id) => {
    console.log(id)
    return await User.updateMany(
        {$pullAll: {"itemList": [id], "bin": [id]}},
    )
};

exports.saveUser = async (user) => {
    return await user.save()
}

exports.sendNewCode = async (email, code) => {
    return await User.findOneAndUpdate({email: email},
        {
            $set:
                {
                    confirmationCode: code,
                    lastConfirmationCodeUpdate: moment(),
                }
        }, {new: true})
};

exports.updatePassword = async (email, password) => {
    return await User.findOneAndUpdate({email: email}, {$set: {passwordHash: password}}, {new: true})
};

exports.blockUser = async (id, block) => {
  return await User.findOneAndUpdate({_id: id}, {$set: {block: block}}, {new: true})
};

exports.getUsers = async () => {
  return await User.find({})
};