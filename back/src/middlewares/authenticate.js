import jwt from "jsonwebtoken";
var User = require("../models/User");

export default (req, res, next) => {
    const header = req.headers.authorization;
    let token;

    if (header) token = header.split(" ")[1];

    if (token) {
        jwt.verify(token.substring(1, token.length - 1), process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                res.status(401).json({ errors: { global: "Invalid token" } });
            } else {
                User.findOne({ email: decoded.email }).then(user => {
                    req.currentUser = user;
                    next();
                });
            }
        });
    } else {
        res.status(401).json({ errors: { global: "No token" } });
    }
};