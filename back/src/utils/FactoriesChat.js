const initialChat = {rooms: []};

const action = (type, value) => {

    switch (type) {
        case "CREATE_CHAT": {
            if(initialChat.rooms[`${value}`] === undefined) {
                initialChat.rooms[`${value}`] = {messages: []};
                return initialChat.rooms[value];
            } else {
                return initialChat.rooms[value];
            }
        }
        case "MESSAGE_SEND": {
            value.data.date = new Date();
            initialChat.rooms[`${value.activeRoom}`].messages.push(value.data);
            return initialChat.rooms[value.activeRoom]
        }
        default: break
    }
};



module.exports = {
    action
};