const io = require('../index.js').io;

const {
    USER_DISCONNECTED,
    MESSAGE_RECIEVED,
    MESSAGE_SEND,
    LOGOUT, CREATE_CHAT
} = require('./ChatEvents');
let members = [];
const {action} = require('./FactoriesChat');


module.exports = (socket) => {
    console.log("Socket Id:" + socket.id);
    members.push(socket.id);
    //User disconnects
    socket.on(LOGOUT, () => {
        socket.disconnect();
    });

    socket.on(CREATE_CHAT, (callback) => {
        const response = action(CREATE_CHAT, callback.room, socket.id);
        socket.emit(MESSAGE_SEND, response.messages);
        members.forEach(item => {
            socket.to(response.userId).emit(MESSAGE_SEND, response);
        });

    });

    socket.on(MESSAGE_SEND, (data) => {
        const response = action(MESSAGE_SEND, data);
        socket.emit(MESSAGE_SEND, response);
        members.forEach(item => {
            socket.to(item).emit(MESSAGE_SEND, response);
        });
    });

};