const _ = require("lodash");

exports.module = function(errors) {
    const result = {};
    _.forEach(errors, (val, key) => {
        result[key] = val.message;
    });
    return result;
}

exports.catchError =  function(prevError, nextError) {
    if (prevError.errors.message == undefined) {
        return prevError
    } else {
        return nextError
    }
};