const jwt = require("jsonwebtoken");

exports.getDecodetToken  = async (token) => {
    let email;
    await jwt.verify(token.substring(1, token.length - 1), process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            email = {errors: {message: err}}
        } else {
            email = decoded.email
        }
    });
    return email
};