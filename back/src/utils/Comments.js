var User = require("../models/User");

exports.getCommentObj = async function(comments){
    if (comments.comment.length > 0) {
        for (let i = 0; comments.comment.length > i; i++) {
            await User.findOne({ _id: `${comments.comment[i].ownerId }`}).then(user => {
                if(user === null) {
                    comments.comment[i].name = 'unknown user'
                } else {
                    comments.comment[i].name = `${user.first_name} ${user.last_name}`
                }
            })
        }
    }

    let ParentItems = comments.comment.filter(item => item.parentId === null);
    let ChildItems = comments.comment.filter(item => item.parentId !== null);
    let responseComment = ParentItems;
    responseComment.forEach(item => {
        if (ChildItems.length > 0) {
            ChildItems.forEach(child => {
                if(item.id == child.parentId) {
                    if(item.subComment === undefined) {
                        item.subComment = [];
                        item.subComment.push(child);
                    } else {
                        item.subComment.push(child);
                    }
                }
            })
        } else {
            return responseComment
        }

    });
    return responseComment;
};