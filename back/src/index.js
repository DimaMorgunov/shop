import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import Promise from "bluebird";
import item from './routes/controller/item';
import image from './routes/controller/image';
import user from './routes/controller/user';
import comments from './routes/controller/comment';
import { parseUrlItems} from "./routes/service/Parser";
var schedule = require('node-schedule');
var log4js = require('log4js');


const SocketManager = require('./utils/SochetManager');

dotenv.config();
const app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));

log4js.configure({
    appenders: {
        info: { type: 'dateFile', filename: 'log/info.log', pattern: 'yyyy-MM-dd', alwaysIncludePattern: true, keepFileExt: true, daysToKeep: 30 },
        app: { type: 'dateFile', filename: 'log/app.log', pattern: 'yyyy-MM-dd', alwaysIncludePattern: true, keepFileExt: true, daysToKeep: 30 },
        errorFile: { type: 'dateFile', filename: 'log/errors.log', pattern: 'yyyy-MM-dd', alwaysIncludePattern: true, keepFileExt: true, daysToKeep: 30 },
        errors: { type: 'logLevelFilter', level: 'error', appender: 'errorFile' },
        console: { type: 'console' },
    },
    categories: {
        default: { appenders: ['info', 'errors', 'console'], level: 'info' },
    }
});


var logger = log4js.getLogger();
logger.level = 'debug';
logger.level = 'warn';
logger.level = 'info';

app.use(bodyParser.json({limit: '50mb'}));

mongoose.Promise = Promise;
mongoose.connect(process.env.MONGODB_URL, { useMongoClient: true });

app.use("/api/user", user);
app.use("/api/item", item);
app.use("/api/image", image);
app.use("/api/comments", comments);

var server = require('http').createServer(app);
var io = module.exports.io = require('socket.io')(server);

io.on('connection', SocketManager);


/*var j = schedule.scheduleJob('* * 6 * * *', parseUrlItems);*/
/*parseUrlItems()*/

server.listen(8080, () => console.log("Running on localhost:8080"));
export default logger;