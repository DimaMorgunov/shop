var mongoose = require('mongoose');

const schema = new mongoose.Schema({

    url: { type: String },
    title: { type: String, required: true },
    price: { type: String, required: true },

    description: {type: Object},
    characteristic: {type: Object},
    type: {type: String, enum: ['laptop', 'phone', 'monoblock', 'monitor', 'accessories', 'other'], required: true},

    updated: { type: Date, default: Date.now, required: true },
    stars: {type: [mongoose.Schema.Types.Mixed]},
    owner: {type: String, required: true}
});

module.exports = mongoose.model("Item", schema);