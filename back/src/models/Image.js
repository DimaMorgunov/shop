var mongoose = require('mongoose');

const schema = new mongoose.Schema({

    imageBinary: {type: Buffer, contentType: String, required: true},
    imagesUrl: {type: String},
    imageId: {type: String, required: true},

});

module.exports =  mongoose.model("Image", schema);