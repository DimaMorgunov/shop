const mongoose = require("mongoose");
const bcrypt =  require("bcrypt");
const jwt = require("jsonwebtoken");
const uniqueValidator = require("mongoose-unique-validator");
const {confirmationEmail} = require('../utils/emailTamplate');

const schema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: true,
            index: true,
            unique: true
        },
        passwordHash: { type: String, required: true },
        role: { type: String, enum: ['admin', 'user'] , default: 'admin' },
        avatar: {type: String},
        first_name: { type: String, required: true },
        last_name: { type: String, required: true },
        itemList: { type: [String] },
        bin: { type: [String] },
        blocked: { type: Boolean, default: false, required: true},

        confirmationCode: {type: String},
        confirmation: {type: Boolean, default: false, required: true },
        lastConfirmationCodeUpdate: { type: Date, default: Date.now, required: true },
    },
    { timestamps: true }
);

schema.methods.isValidPassword = function isValidPassword(password) {
    return bcrypt.compareSync(password, this.passwordHash);
};

schema.methods.setPassword = function setPassword(password) {
    this.passwordHash = bcrypt.hashSync(password, 10);
};

schema.methods.generateJWT = function generateJWT() {
    return jwt.sign(
        {
            email: this.email,
        },
        process.env.JWT_SECRET
    );
};

schema.methods.setConfirmCode = function setConfirmCode() {
    this.confirmationCode = `${Math.floor((Math.random() * 1000000) + 54)}`.substring(0,5);
    console.log(this.email, this.confirmationCode)
    confirmationEmail(this.email, this.confirmationCode)
};

schema.methods.toAuthJSON = function toAuthJSON() {
    return {
        data: {
            email: this.email,
            first_name: this.first_name,
            last_name: this.last_name,
            itemList: this.itemList,
            wishList: this.wishList,
            bin: this.bin,
            role: this.role,
            _id: this._id,
            confirmation: this.confirmation,

        },
        token: this.generateJWT()
    };
};

schema.plugin(uniqueValidator, { message: "This email is already taken" });

module.exports = mongoose.model("User", schema);