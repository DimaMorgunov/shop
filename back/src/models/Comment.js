var mongoose = require('mongoose');

const schema = new mongoose.Schema({

    idItem: {type: String, required: true},
    comment: {type: [mongoose.Schema.Types.Mixed]},
    date: {type: Date, default: Date.now, required: true},


}, { usePushEach: true });

module.exports = mongoose.model("Comment", schema);