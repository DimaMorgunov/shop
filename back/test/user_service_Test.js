const user = require('../src/routes/service/user');
let testData = require('./test');
const mocha = require('mocha');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
testData = testData.modules;

describe('registerUser', function () {
    it('register user', async function () {
        console.log(testData)
        const requestData = {
            first_name: 'Test',
            last_name: 'Testovich',
            email: 'test@test.com',
            password: 'test',
            photo: {photoBase64: testData.imageBase64}
        };
        const responseData = await user.registerUser(requestData);
        testData.token = responseData.user.token;
        testData.userData = responseData.user.data;
        expect(`${requestData.email}`).to.equal(`${responseData.user.data.email}`);
    })
});

describe('loginUser', function () {
    it('login user', async function () {
        const requestData = {
            email: 'test@test.com',
            password: 'test',
        };
        const responseData = await user.loginUser(requestData);
        testData.token = responseData.user.token;
        testData. userData = responseData.user.data;
        expect(`${requestData.email}`).to.equal(`${responseData.user.data.email}`);
    })
});

describe('reset password', function () {
    it('reset password', async function () {
        const requestData = {
            oldPassword: 'test',
            newPassword: 'newPasswordTest',
            repeatNewPassword: 'newPasswordTest'
        };
        const responseData = await user.resetPassword(`Bearer '${testData.token}'`,requestData);
        responseData.success.message.should.be.a('string')
    })
});

describe('lost password', function () {
    it('lost password', async function () {
        const requestData = {
            email: testData.userData.email,
        };
        const responseData = await user.lostPassword(requestData);
        responseData.success.message.should.be.a('string')
    })
});

describe('socialAuth', function () {
    it('social auth', async function () {
        const requestData = {
            email: "greewas111@gmail.com",
            id: "114179693147594",
            name: "Dima Morgunov"
        };
        const responseData = await user.authSocial(requestData);
        testData.token = responseData.user.token;
        testData.userData = responseData.user.data;
        expect(`${requestData.email}`).to.equal(`${responseData.user.data.email}`);
    })
});

describe('update profile', function () {
    it('update profile', async function () {
        const requestData = {
            first_name: 'Test',
            last_name: 'Testovich',
            photo: {photoBase64: ''}
        };
        const responseData = await user.updateProfile(`Bearer '${testData.token}'`, requestData);
        testData.token = responseData.user.token;
        testData.userData = responseData.user.data;
        expect(`${requestData.first_name}`).to.equal(`${responseData.user.data.first_name}`);
    })
});

describe('check code', function () {
    it('check code', async function () {
        const requestData = {
            num: '2123A',
        };
        const responseData = await user.checkCode(`Bearer '${testData.token}'`,requestData);
        expect(`${testData.userData.email}`).to.equal(`${responseData.user.data.email}`);
    })
});

describe('get code', function () {
    it('get code', async function () {
        const requestData = {
            email: testData.userData.email,
        };
        const responseData = await user.getCode(`Bearer '${testData.token}'`,requestData);
        responseData.success.message.should.be.a('string')
    })
});

describe('get users', function () {
    it('get users', async function () {
        const responseData = await user.getUsers(`Bearer '${testData.token}'`);
        responseData.should.be.a('array')
    })
});