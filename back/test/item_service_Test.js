const item = require('../src/routes/service/item');
const comment = require('../src/routes/service/comment');
const user = require('../src/routes/service/user');
const image = require('../src/routes/service/image');

let testData = require('./test');
const mocha = require('mocha');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;
testData = testData.modules;


    describe('add Item', function () {
        it('add Item', async function () {
            const requestData = {
                title: 'Test item',
                price: '10000',
                type: 'phone',
                characteristic: [],
                description: [],
                photo: {uri: '', photoBase64: testData.imageBase64},
                owner: `${testData.userData.first_name} ${testData.userData.last_name}`,
                email: testData.userData.email
            };
            const responseData = await item.addItem(requestData);
            testData.userData = responseData.user.data;
            expect(`${testData.userData.email}`).to.equal(`${responseData.user.data.email}`);
        })
    });

    describe('get Item', function () {
        it('get Item', async function () {
            const responseData = await item.getCurrentItem(testData.userData.itemList[0]);
            testData.testItem = responseData[0];
            expect(`${testData.userData.itemList[0]}`).to.equal(`${responseData[0]._id}`);
        })
    });


    describe('get user items', function () {
        it('get user items', async function () {
            const responseData = await item.getUserItems(testData.userData.itemList);
            testData.testItem = responseData[0];
            responseData.should.be.a('array')
        })
    });

    describe('update item', function () {
        it('update item', async function () {
            const requestData = {
                title: 'update Test item',
                price: '20000',
                type: 'other',
                characteristic: [],
                description: [],
                photo: {uri: '', photoBase64: testData.imageBase64},
                _id: testData.testItem._id
            };
            const responseData = await item.updateItem(`'${testData.token}'`,requestData);
            responseData.success.message.should.be.a('string')
        })
    });

    describe('add star', function () {
        it('add star', async function () {
            const requestData = {
                id: testData.userData.itemList[0],
                rating: 4
            };
            const responseData = await item.addStar(`'${testData.token}'`,requestData);
            expect(`${testData.userData.itemList[0]}`).to.equal(`${responseData._id}`);
        })
    });

    describe('add comment', function () {
        it('add comment', async function () {
            const requestData = {
                email: testData.userData.email,
                itemId: testData.testItem._id,
                comment: 'test comment'
            };
            const responseData = await comment.addComment(requestData);
            testData.testComments = responseData;
            expect(`${requestData.comment}`).to.equal(`${responseData[0].text}`);
        })
    });

    describe('add subcomment', function () {
        it('add subcomment', async function () {
            const requestData = {
                email: testData.userData.email,
                itemId: testData.testItem._id,
                subComment: 'test subcomment',
                number: `${testData.testComments[0].id}`
            };
            const responseData = await comment.addSubComment(requestData);
            responseData.should.be.a('array')
        })
    });

    describe('get comment', function () {
        it('get comment', async function () {
            const responseData = await comment.getComment(testData.testItem._id);
            responseData.should.be.a('array')
        })
    });

    describe('update comment', function () {
        it('update comment', async function () {
            const requestData = {
                email: testData.userData.email,
                itemId: testData.testItem._id,
                editText: 'test update comment',
                index: `${testData.testComments[0].id}`
            };
            const responseData = await comment.updateComment(requestData);
            responseData.should.be.a('array')
        })
    });

    describe('update comment', function () {
        it('update comment', async function () {
            const requestData = {
                email: testData.userData.email,
                itemId: testData.testItem._id,
                editText: 'test update comment',
                index: `${testData.testComments[0].id}`
            };
            const responseData = await comment.updateComment(requestData);
            responseData.should.be.a('array')
        })
    });

    describe('delete comment', function () {
        it('delete comment', async function () {
            const requestData = {
                email: testData.userData.email,
                itemId: testData.testItem._id,
                id: `${testData.testComments[0].id}`
            };
            const responseData = await comment.deleteComment(requestData);
            responseData.should.be.a('array')
        })
    });

    describe('add to bin', function () {
        it('add to bin', async function () {
            const responseData = await user.addToBin(`Bearer '${testData.token}'`, `${testData.testComments[0].id}`);
            testData.userData = responseData.user.data;
            expect(`${responseData.user.data.bin[0]}`).to.equal(`${testData.testComments[0].id}`);
        })
    });

    describe('remove from bin', function () {
        it('remove from bin', async function () {
            const responseData = await user.removeFromBin(`'${testData.token}'`, {itemId: `${testData.testComments[0].id}`});
            expect(`${responseData.data.bin.length}`).to.equal('0');
        })
    });

    describe('get image', function () {
        it('get image', async function () {
            const responseData = await image.getImage(testData.testItem._id);
            expect(`${responseData[0].imageId}`).to.equal(`${testData.testItem._id}`);
        })
    });

    describe('delete item', function () {
        it('delete item', async function () {
            const responseData = await item.deleteItem(`${testData.testItem._id}`);
            console.log(responseData)
            responseData.success.message.should.be.a('string')
        })
    });





