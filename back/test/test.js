const dotenv = require('dotenv');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");

dotenv.config();

mongoose.connect(process.env.MOGODB_TEST, {useMongoClient: true});
mongoose.Promise = global.Promise;
mongoose.connection
    .once('open', () => console.log('Connected!'))
    .on('error', (error) => {
        console.warn('Error : ', error);
    });




const mocha = require('mocha');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;

const item = require('../src/routes/service/item');
const image = require('../src/routes/service/image');
const comment = require('../src/routes/service/comment');

let imageBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAQ5JREFUeNpi/P//PwMlgImBQjDwBrCgCxj1XGfg4OZmYGNnj2FgZCxg+P9/wq+fP5f8+PqV4VyJJnEuAAZsDFBTQZS7mDGIBvGJ9gJI8c9v3wri/OWMX/xgYIj2kzMG8XEZgmHAz+/fbb9/+cIwcdbps4+/MzBMmX36LIgPEicqDP7/+5f+++dPht+/fp55+JWB4dvnTwysbOwmrOzsxAXi148fGUA2gsDrn0ADPn0GsoD4zjYgbYo1wFAw2FRxLQbuyCVndA7+/w+iQXxsakGYBZuz/ry8pvH/8YVbN/q+Mfx/e+vW35fXjIDC14D4B7paRvS8wMjICKJEgJgN2aEgHwHV/iFowNDLCwABBgC9qJ54WqC2JwAAAABJRU5ErkJggg=='
let token = '';
let userData = {};
let testItem = {};
let testComments = {};

exports.modules = {token, imageBase64, userData, testItem, testComments};

describe('V1 SERVICE', () => {

    before(function (done) {

        function clearCollections() {
            for (var collection in mongoose.connection.collections) {
                mongoose.connection.collections[collection].remove(function() {});
            }
            return done();
        }

        if (mongoose.connection.readyState === 0) {
            mongoose.connect(config.test.db, function (err) {
                if (err) throw err;
                return clearCollections();
            });
        } else {
            return clearCollections();
        }
    });

    require('./user_service_Test');
});

describe('V2 SERVICE', () => {
    require('./item_service_Test');
});

/*describe('V3 SERVICE', () => {
    require('./image_service_Test');
});

describe('V4 SERVICE', () => {
    require('./comment_service_Test');
});*/


